package main

import (    
    "testing"    
    "encoding/json"
    //"github.com/kataras/iris"
)

// TestTemplateAndMethod - тестируем шаблон из JSON 
func TestTemplateAndMethod(t *testing.T) {
    template := TemplateExchangeMethod{ ID: -1, Name: "Number Template", Online: true,
        Validator: "{\"test\":\"^(?:4[0-9]{12}(?:[0-9]{3})?|(?:5[1-5][0-9]{2}))$\"}", ValidatorJSON: make(map[string]interface{},0)}

    err := json.Unmarshal([]byte(template.Validator), &template.ValidatorJSON)
    if err != nil {
        t.Error(err.Error())
        return
    }

    method := ExchangeMethod{ ID: -1, Description: "My Number Template", TemplateID: -1, Template: &template,
        Requisites: "{\"test\":\"4111111111111111\"}", RequisitesJSON: make(map[string]interface{},0)}

    err = json.Unmarshal([]byte(method.Requisites), &method.RequisitesJSON)
    if err != nil {
        t.Error(err.Error())
        return
    }

    err = template.MethodValidation(&method)
    if err != nil {
        t.Fail()
        return
    }    
}