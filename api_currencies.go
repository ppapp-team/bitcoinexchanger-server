package main

import (                
    "github.com/kataras/iris"    
)

/* Модели запросов */

/* Запросы */

// APIGetCurrenciesV1 - получаем список фиатной валюты из БД
func APIGetCurrenciesV1(c *iris.Context) {
    var db *Database
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)        
        if orm != nil {
            urlParams := c.QueryArgs()            
            var list []Currency
            // Обработка фильтров
            ids := GetIntArrayFromFilter("id", urlParams)
            if len(ids) > 0 {
                orm = orm.Where(ids)
            }            
            codes := GetStrArrayFromFilter("code", urlParams)
            if len(codes) > 0 {
                orm = orm.Where("code in (?)",codes)
            }            
            // Выборка с названиями фиатных валют
            if !urlParams.Has("withOutNames") {
                locales := GetStrArrayFromFilter("locale", urlParams)
                if len(locales) > 0 {
                    orm = orm.Preload("Names", "language_locale in (?)", locales)
                } else {
                    orm = orm.Preload("Names")
                }                   
            }  
            orm.Find(&list)
            c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":list})
            return
        }
    }
    // Сервис недоступен
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

// APIGetCurrencyNamesV1 - получение имен
func APIGetCurrencyNamesV1(c *iris.Context) {    
    var db *Database
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)        
        if orm != nil {
            urlParams := c.QueryArgs()
            id, err := c.ParamInt64("id")
            if err != nil {
                c.JSON(iris.StatusBadRequest, iris.Map{"error":NewAPIError(29, []APIErrorCause{APIErrorCause{Target:"id", Сause:err.Error()}})})
                return
            }
            var list []CurrencyName
            locales := GetStrArrayFromFilter("locale", urlParams)
            if len(locales) > 0 {
                orm = orm.Where("language_locale in (?)", locales)
            }
            orm.Where("currency_id = ?", id).Find(&list)
            c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":list})
            return
        }
    }        
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

/* Инициализация API */

// InitAPICurrencies - инициализация API для фиатной валюты
func InitAPICurrencies(api iris.MuxAPI, version int) {
    // Для первой версии
    currencies := api.Party("/currencies")
    if version == 1 {
        /**
        * @api {get} /v1/currencies Получение списка фиатной валюты с текущими курсами BTC
        * @apiName GetListCurrencies
        * @apiGroup Currencies
        * @apiVersion 0.0.1
        * @apiPermission all
        *                        
        * @apiParam {Number[]} [filter[id]] Фильтр по идентификаторам (Условие: id in (filter[id]))        
        * @apiParam {String[]} [filter[code]] Фильтр по трехзначным кодам фиатной валюты (Условие: code in (filter[code]))
        * @apiParam {Number} [withOutNames] Если присутствует в запросе, то будет работать фильтр по языкам
        * @apiParam {String[]} [filter[locale]] Фильтр по языкам названия валюты
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object[]} result Объекты фиатной валюты (model: Currency)
        * @apiSuccess (200) {Number} result.id Идентификатор фиатной валюты
        * @apiSuccess (200) {Object[]} result.names Названия фиатной валюты в соответсвии с языками (model: CurrencyName)
        * @apiSuccess (200) {String} result.names.language_locale Язык (rus, eng...)
        * @apiSuccess (200) {String} result.names.title Название
        * @apiSuccess (200) {String} result.code Трех символьный код фиатной валюты (RUS, EUR...)
        * @apiSuccess (200) {String} result.symbol Unicode символ фиатной валюты ($...)
        * @apiSuccess (200) {Number} result.buy_course Текущий курс BTC на покупку
        * @apiSuccess (200) {Number} result.sell_course Текущий курс BTC на продажу       
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "result": [{
        *            "id": 1,
        *            "names": [{
        *                "language_locale": "rus",
        *                "title": "Русский рубль"
        *            }, {
        *                "language_locale": "eng",
        *                "title": "Russian ruble"
        *            }],
        *            "code": "RUB",
        *            "symbol": "₽",
        *            "buy_course": 35753.1,
        *            "sell_course": 35819.78
        *        }, {
        *            "id": 2,
        *            "names": [{
        *                "language_locale": "rus",
        *                "title": "Американский доллар"
        *            }, {
        *                "language_locale": "eng",
        *                "title": "American dollar"
        *            }],
        *            "code": "USD",
        *            "symbol": "$",
        *            "buy_course": 536.22,
        *            "sell_course": 537.22
        *        }, {
        *            "id": 3,
        *            "names": [{
        *                "language_locale": "rus",
        *                "title": "Евро"
        *            }, {
        *                "language_locale": "eng",
        *                "title": "Euro"
        *            }],
        *            "code": "EUR",
        *            "symbol": "€",
        *            "buy_course": 477.75,
        *            "sell_course": 478.64
        *        }]
        *    }
        */
        currencies.Get("", APIGetCurrenciesV1)

        /**
        * @api {get} /v1/currencies/:id/names Получение списка названий фиатной валюты
        * @apiName GetListNamesFromCurrencyByID
        * @apiGroup Currencies
        * @apiVersion 0.0.1
        * @apiPermission all
        *
        * @apiParam {Number} id Идентификатор фиатной валюты      
        * @apiParam {String[]} [filter[locale]] Фильтр по языкам названия валюты
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object[]} result Объекты фиатной валюты (model: CurrencyName)        
        * @apiSuccess (200) {String} result.language_locale Язык (rus, eng...)
        * @apiSuccess (200) {String} result.title Название               
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "result": [{
        *            "language_locale": "rus",
        *            "title": "Русский рубль"
        *        }, {
        *            "language_locale": "eng",
        *            "title": "Russian ruble"
        *        }]
        *    }        
        */
        currencies.Get("/:id/names", APIGetCurrencyNamesV1)                  
    }
}