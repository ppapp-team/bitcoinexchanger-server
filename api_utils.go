package main

import (
    "time"    
    "strings"
    "strconv"
    "github.com/valyala/fasthttp"
)

// GetOrderDirect - получить направление сортировки
func GetOrderDirect(value bool) string {
    if value {
        return "desc"
    }
    return "asc"
} 

// GetStrArrayFromFilter - получить список string значений по ключу из фильтров в URL
func GetStrArrayFromFilter(key string, params *fasthttp.Args) []string {
    var result []string
    if params != nil {
        if params.Has("filter["+key+"]") {
            result = strings.Split(string(params.Peek("filter["+key+"]")),",")
            for i := 0; i < len(result); i++ {
                result[i] = strings.TrimSpace(result[i])
            }
        } else if params.Has("filter["+key+"][]") {
            for _, val := range params.PeekMulti("filter["+key+"][]") {
                result = append(result, strings.TrimSpace(string(val)))
            }
        }
    }
    return StrRemoveDuplicates(result)  
}

// GetIntArrayFromFilter - получить список int значений по ключу из фильтров в URL
func GetIntArrayFromFilter(key string, params *fasthttp.Args) []int {
    var result []int
    if params != nil {
        var list []string
        if params.Has("filter["+key+"]") {
            list = strings.Split(string(params.Peek("filter["+key+"]")),",")
        } else if params.Has("filter["+key+"][]") {
            for _, val := range params.PeekMulti("filter["+key+"][]") {
                list = append(list, string(val))
            }
        }
        
        if len(list) > 0 {
            for _, item := range list {
                val, err := strconv.Atoi(strings.TrimSpace(item))
                if err != nil {
                    continue
                }
                result = append(result, val)
            }
        }
    }
    return IntRemoveDuplicates(result)
}


/* TIMER */

// NewTimer - Создание таймеров
func NewTimer(tick time.Duration, action func()) *time.Timer {
    timer := time.NewTimer(tick)
    go func() {
        <-timer.C
        action() 
    }()   
    return timer
}