package main

import (
    "strings"    
    "github.com/kataras/iris"
    "github.com/valyala/fasthttp"    
)

/* Дополнительные функции */

// GetArgsValue - получение значения из аргументов
func GetArgsValue(args *fasthttp.Args, key string) string {
    if args != nil {
        if args.Has(key) {
            return string(args.Peek(key))
        }        
    }
    return ""
}

// JSONToken стрктурса с токеном
type JSONToken struct {
    Token string `json:"token" form:"token"`
}

/* Middleware */

// APIAuthMiddleware - авторизация по токену
func APIAuthMiddleware(c *iris.Context) {    
    // Поиск токена в заголовке / url / post params    
    token := StrDef(c.RequestHeader("Token"), StrDef(c.RequestHeader("token"),StrDef(GetArgsValue(c.QueryArgs(),"token"), StrDef(GetArgsValue(c.PostArgs(),"token"), c.PostValue("token"))))) 
    if len(token) <= 0 {
        // Поиск в JSON
        var json JSONToken
        err := c.ReadJSON(&json)
        if err == nil {
            token = strings.TrimSpace(json.Token)
        }
    }
    // Если токен есть то получаем его из БД 
    // и кладем в Iris
    if len(token) > 32 {
        var db *Database
        if c.Get("database") != nil {
            db = c.Get("database").(*Database)
        }
        if db != nil {
            orm := db.GetORM(false)
            if orm != nil {
                var t Token
                orm.Where("token = ?", token).First(&t)
                if t.ID > 0 && t.Closed == nil {
                    c.Set("token", &t)
                }
            }
        }
    }
    // Дальше
    c.Next()
}