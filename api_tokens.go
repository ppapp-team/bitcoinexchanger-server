package main

import (    
    "strings"    
    "github.com/jinzhu/gorm"
    "github.com/kataras/iris"
    "github.com/legion-zver/utils/unixtime"
)

/* Для БД */

// GenNewTokens - Генерация уникальных токенов
func GenNewTokens(orm *gorm.DB) (string, string) {
    var t       string // sha1 токен
    var rt      string // sha1 рефреш токен 
    var count   int    // число совпадений в БД
    for {              // Генерация уникального токена и рефрештокена
        //fmt.Println("[DEBUG] GENERATE TOKENS")
        orm.Raw("SELECT SHA1(UUID()), SHA1(UUID());").Row().Scan(&t, &rt)
        orm.Model(&Token{}).Where("token = ? OR refresh_token = ?", t, rt).Count(&count)
        if count <= 0 {
            break
        }
    }
    return strings.ToLower(t),strings.ToLower(rt)
}

/* Модели запросов */

// RequestInitTokenV1 - запрос инициализации токена
type RequestInitTokenV1 struct {
    DeviceType  string  `form:"device_type" json:"device_type"` 
    DeviceID    string  `form:"device_id" json:"device_id"`
    Hash        string  `form:"hash" json:"hash"`
    Phrase     *string  `form:"phrase" json:"phrase"`
} 

// Validation - валидация RequestInitTokenV1 
func (r *RequestInitTokenV1) Validation() bool {
    //fmt.Println("[DEBUG]",strings.ToLower(r.Hash)," == ",strings.ToLower(StrToMd5("bitcoin-"+r.DeviceID+"-"+r.DeviceType+"-exchanger-v1")))
    return (strings.ToLower(r.Hash) == strings.ToLower(StrToMd5("bitcoin-"+r.DeviceID+"-"+r.DeviceType+"-exchanger-v1")))
}

// RequestRefreshTokenV1 - запрос обновления токена
type RequestRefreshTokenV1 struct {
    RefreshToken  string  `form:"refresh_token" json:"refresh_token"`     
} 

// Validation - валидация RequestRefreshTokenV1 
func (r *RequestRefreshTokenV1) Validation() bool {
    r.RefreshToken = strings.ToLower(r.RefreshToken)
    return len(r.RefreshToken) > 32 && len(r.RefreshToken) <= 40
}

// RequestChangePushTokenV1 - запрос изменения пуш токена
type RequestChangePushTokenV1 struct {
    PushToken  *string  `form:"push_token" json:"push_token"`     
} 

// Validation - валидация RequestChangePushTokenV1 
func (r *RequestChangePushTokenV1) Validation(DeviceType string) bool {
    if r.PushToken != nil {
        if DeviceType == "android" {
            return len(*r.PushToken) > 128
        } else if DeviceType == "ios" {
            return len(*r.PushToken) == 64
        }
        return false
    }
    return true
}

/* Запросы */

// APIRequestInitTokenV1 - первая версия получения токена доступа
func APIRequestInitTokenV1(c *iris.Context) {
    var db *Database
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)
        if orm != nil {
            var request RequestInitTokenV1
            err := c.ReadJSON(&request)
            if err != nil {
                err = c.ReadForm(&request)
                if err != nil {
                    // Не нашли данных для запроса
                    c.JSON(iris.StatusBadRequest, iris.Map{"error":NewSimpleAPIError(28)})
                    return
                }
            }
            if request.Validation() {
                var token Token                
                // Поиск токена в БД
                orm.Where("device_id = ? AND device_type = ? AND closed IS NULL", request.DeviceID, request.DeviceType).                    
                    Preload("User.Wallet").
                    First(&token)

                if token.ID <= 0 {
                    token = Token{ DeviceID: strings.ToLower(request.DeviceID), DeviceType: strings.ToLower(request.DeviceType)}
                    token.Token, token.RefreshToken = GenNewTokens(orm)
                    if request.Phrase != nil {                        
                        var user User
                        orm.Where("unique_phrase = ?", StrToSha1(*request.Phrase)).First(&user)
                        if user.ID > 0 {
                            token.UserID = user.ID
                            token.User = &user
                        }
                    }
                    if token.User == nil {
                        token.User = new(User)
                    }                    
                    // Данные о времени создания и изменения
                    token.Created = unixtime.Now()
                    token.Updated = token.Created
                    // Создаем токен
                    orm.Create(&token)
                    if len(token.User.UUID) < 24 {
                        orm.Model(token).Related(token.User, "User")
                    }
                } else {                                        
                    token.Token, token.RefreshToken = GenNewTokens(orm)
                    token.Updated = unixtime.Now()
                    if token.User == nil {
                        token.User = new(User)
                    }     
                    orm.Save(&token)
                    if len(token.User.UUID) < 24 {
                        orm.Model(token).Related(token.User, "User")
                    }
                }
                c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":token})
                return
            }
            // Не прошли проверку валидации, запрет
            c.JSON(iris.StatusForbidden, iris.Map{"error":NewSimpleAPIError(29)})
            return
        }
    }
    // Сервис недоступен
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})    
}

// APIRequestRefreshTokenV1 - обновление токена
func APIRequestRefreshTokenV1(c *iris.Context) {
    var db *Database
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)
        if orm != nil {
            var request RequestRefreshTokenV1
            err := c.ReadJSON(&request)
            if err != nil {
                err = c.ReadForm(&request)
                if err != nil {
                    // Не нашли данных для запроса
                    c.JSON(iris.StatusBadRequest, iris.Map{"error":NewSimpleAPIError(28)})
                    return
                }
            }
            if request.Validation() {
                var token Token
                orm.Where("refresh_token = ? AND closed IS NULL", request.RefreshToken).                    
                    Preload("User.Wallet").
                    First(&token)
                    if token.ID > 0 {
                        token.Token, token.RefreshToken = GenNewTokens(orm)
                        token.Updated = unixtime.Now()                        
                        if token.User == nil {
                            token.User = new(User)
                        }
                        orm.Save(&token)
                        if len(token.User.UUID) < 24 {
                            orm.Model(token).Related(token.User, "User")
                        }
                        c.JSON(iris.StatusOK, iris.Map{"error":nil, "result":token})                
                    } else {
                        // Ошибка с причиной
                        c.JSON(iris.StatusForbidden, iris.Map{"error":NewAPIError(44, []APIErrorCause{
                            APIErrorCause{Target: "refresh_token", Сause: "Does not exist"}})})
                    }
                    return
            }
            // Не прошли проверку валидации, запрет
            c.JSON(iris.StatusForbidden, iris.Map{"error":NewSimpleAPIError(29)})
            return
        }
    }
    // Сервис недоступен
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

// APIRequestChangePushTokenV1 - изменение пуштокена
func APIRequestChangePushTokenV1(c *iris.Context) {
    // База данные, токен доступа
    var db      *Database    
    var token   *Token

    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)
        if orm != nil {
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)
            }
            if token != nil {
                var request RequestChangePushTokenV1
                err := c.ReadJSON(&request)
                if err != nil {
                    err = c.ReadForm(&request)
                    if err != nil {
                        // Не нашли данных для запроса
                        c.JSON(iris.StatusBadRequest, iris.Map{"error":NewSimpleAPIError(28)})
                        return
                    }
                }
                if request.Validation(strings.ToLower(token.DeviceType)) {
                    token.PushToken = request.PushToken
                    token.Updated = unixtime.Now()
                    
                    orm.Save(token)

                    c.JSON(iris.StatusOK, iris.Map{"error":nil, "result":"success"})
                    return
                }
                // Не прошли проверку валидации, запрет
                c.JSON(iris.StatusForbidden, iris.Map{"error":NewSimpleAPIError(29)})
                return
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    // Сервис недоступен
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

/* Инициализация */

// InitAPITokens - инициализация API для токенов доступа
func InitAPITokens(api iris.MuxAPI, version int) {
    // Для первой версии
    tokens := api.Party("/tokens")
    if version == 1 {        
        /**
        * @api {post} /v1/tokens/init Инициализация устройства и получение токена доступа
        * @apiName InitToken
        * @apiGroup Tokens
        * @apiVersion 0.0.1
        * @apiPermission all
        *
        * @apiParam {String} device_type Тип устройства ('ios','android','wp','browser','other')
        * @apiParam {String} device_id Уникальный идентификатор устройства
        * @apiParam {String} hash Специально расчитанный хеш - для подтверждения
        * @apiParam {String} [phrase] Уникальная фраза - идентификатор учетной записи, для автоматического входа с другого устройства под своей учетной записью. Если ее нет в запросе и устройство первый раз проходит инициализацию, то будет создана новая пустая учетная запись пользователя
        *
        * @apiSuccess (201) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (201) {Object} result Результат - объект Token
        * @apiSuccess (201) {String="ios","android","wp","browser","other""} result.device_type Тип устройства
        * @apiSuccess (201) {String} result.device_id Уникальный идентификатор устройства
        * @apiSuccess (201) {String} result.push_token=null Push токен устройства для рассылок уведомлений, если null то не назначен
        * @apiSuccess (201) {String} result.token Уникальный токен доступа (время его жизни 7 дней)
        * @apiSuccess (201) {String} result.refresh_token Уникальный refersh токен для обновления токена доступа (время его жизни 3 месяца / одноразовый)
        * @apiSuccess (201) {Object} result.user Данные о пользователе
        * @apiSuccess (201) {String} result.user.uuid Уникальный идентификатор учетной записи пользователя
        * @apiSuccess (201) {Object} result.user.wallet Кошелек пользователя / Может не быть, если его не было
        * @apiSuccess (201) {String} result.user.wallet.uuid Уникальный идентификатор кошелька
        * @apiSuccess (201) {String} result.user.wallet.bitcoin_address Уникальный bitcoin адрес кошелька
        * @apiSuccess (201) {Number} result.user.wallet.created Дата создания кошелька в формате Unix
        * @apiSuccess (201) {Number} result.user.wallet.updated Дата изменения параметров безопасности кошелька в формате Unix
        * @apiSuccess (201) {Number} result.created Дата создания токена в формате Unix
        * @apiSuccess (201) {Number} result.updated Дата последнего обновления токена в формате Unix
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 201 CREATED
        *    {
        *        "error": null,
        *        "result": {
        *            "device_type": "ios",
        *            "device_id": "test",
        *            "push_token": null,
        *            "token": "7d1705f83c3a298a424519bcd3b818518c6062b3",
        *            "refresh_token": "dd941a37ec5f13e6b54c1a0412ada0e3285aeb3f",
        *            "user": {
        *                "uuid": "a87050d1-48fd-11e6-9bf3-5254003dd900",
        *                "wallet": {
        *                    "bitcoin_address": "miEsRU1D77R4MhtAf1RruBVMEuMJeosiJy",
        *                    "created": 1469485203,
        *                    "uuid": "eec91281-52b5-11e6-9963-5254003dd900",
        *                    "updated": 1469485203
        *                }
        *            },
        *            "created": 1468408990,
        *            "updated": 1470204889
        *        }
        *    }
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 29,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        */     
        tokens.Post("/init", APIRequestInitTokenV1)
        /**
        * @api {post} /v1/tokens/refresh Обновление токена доступа посредством refresh токена
        * @apiName RefreshToken
        * @apiGroup Tokens
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiParam {String} refresh_token Refresh токен        
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object} result Результат - объект Token        
        * @apiSuccess (200) {String="ios","android","wp","browser","other""} result.device_type Тип устройства
        * @apiSuccess (200) {String} result.device_id Уникальный идентификатор устройства
        * @apiSuccess (200) {String} result.push_token=null Push токен устройства для рассылок уведомлений, если null то не назначен
        * @apiSuccess (200) {String} result.token Уникальный токен доступа (время его жизни 7 дней)
        * @apiSuccess (200) {String} result.refresh_token Уникальный refersh токен для обновления токена доступа (время его жизни 3 месяца / одноразовый)
        * @apiSuccess (200) {Object} result.user Данные о пользователе
        * @apiSuccess (200) {String} result.user.uuid Уникальный идентификатор учетной записи пользователя
        * @apiSuccess (200) {Object} result.user.wallet Кошелек пользователя / Может не быть, если его не было
        * @apiSuccess (200) {String} result.user.wallet.uuid Уникальный идентификатор кошелька
        * @apiSuccess (200) {String} result.user.wallet.bitcoin_address Уникальный bitcoin адрес кошелька
        * @apiSuccess (200) {Number} result.user.wallet.created Дата создания кошелька в формате Unix
        * @apiSuccess (200) {Number} result.user.wallet.updated Дата изменения параметров безопасности кошелька в формате Unix
        * @apiSuccess (200) {Number} result.created Дата создания токена в формате Unix
        * @apiSuccess (200) {Number} result.updated Дата последнего обновления токена в формате Unix
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "result": {
        *            "device_type": "ios",
        *            "device_id": "test",
        *            "push_token": null,
        *            "token": "d50528eff16e9599f688f0158ca7d6cbd247dd34",
        *            "refresh_token": "a1873813cd4a0d04758d56d12151594f8189a911",
        *            "user": {
        *                "uuid": "a87050d1-48fd-11e6-9bf3-5254003dd900",
        *                "wallet": {
        *                    "bitcoin_address": "miEsRU1D77R4MhtAf1RruBVMEuMJeosiJy",
        *                    "created": 1469485203,
        *                    "uuid": "eec91281-52b5-11e6-9963-5254003dd900",
        *                    "updated": 1469485203
        *                }
        *            },
        *            "created": 1468408990,
        *            "updated": 1470196278
        *        }
        *    }
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 29,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        */
        tokens.Post("/refresh", APIRequestRefreshTokenV1)
        /**
        * @api {post} /v1/tokens/push-token Установка push токена для уведомлений
        * @apiName SetPushToken
        * @apiGroup Tokens
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {String} push_token Push токен
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {String} result=success Текстовое обозначение результата операции назначения Push токена        
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 29,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "result": "success"
        *    }
        */
        tokens.Post("/push-token", APIRequestChangePushTokenV1)        
    }
}