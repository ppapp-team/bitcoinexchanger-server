package main

import (    
    "fmt"        
    "strconv"    
    "io/ioutil"
    "path/filepath"    
    "encoding/json"    
    "github.com/kataras/iris"
    "github.com/kardianos/osext"    
    "github.com/kataras/iris/config"    
    "github.com/iris-contrib/middleware/cors"    
)

// Config - конфигурация запоска программы
type Config struct {
    Host            string  `json:"host"`
    ListenPort      int     `json:"listen_port"`
    ConfigMySQL     string  `json:"mysql"`
    PublicPath      string  `json:"public-path"`
    DocsPath        string  `json:"docs-path"`
    TemplatesPath   string  `json:"templates-path"`
    RPCHost         string  `json:"rpc-host"`
    RPCUser         string  `json:"rpc-user"`
    RPCPass         string  `json:"rpc-password"`
}

// Стартуем
func main() {
    exeFile, _ := osext.Executable()
    currentDir, err := filepath.Abs(filepath.Dir(exeFile))
    if err != nil {
        fmt.Println("...ERROR\nDescription: ", err)
        return
    }
    fmt.Print("[Bitcoin.Exchanger] Loading configuration")
    data, err := ioutil.ReadFile(currentDir+"/config.json")
    if err != nil {
        fmt.Println("...ERROR\nDescription: ", err)
        return
    }
    var config Config
    err = json.Unmarshal(data, &config)
    if err != nil {
        fmt.Println("...ERROR\nDescription: Unmarshal config file error - ", err)
        return
    }
    fmt.Println("...OK\n[Bitcoin.Exchanger] Running service")
    
    // Инициализация базы данных
    database := Database{}
    if !database.Init(config.ConfigMySQL) {         
        fmt.Println("\t* [WARNING] Not connection to MySQL DB, wait connection...")    
    } else {
        fmt.Println("\t* MySQL is connected")
    }

    // Автомиграция в фоне
    go database.AutoMigrationModels()

    // Инициализация RPC клиента
    var rpcClient BitcoinJSONRPC
    
    if !rpcClient.Init(config.RPCHost, config.RPCUser, config.RPCPass) { 
        fmt.Println("\t* [WARNING] Not connection to BITCOIN RPC JSON SERVER, Bitcoin functions not works!!!")        
    } else {
        fmt.Println("\t* BITCOIN RPC JSON SERVER is connected")
    }

    // Запуск API
    RunAPI(&config, currentDir, &database, &rpcClient, config.ListenPort)  
}

// RunAPI - запуск API
func RunAPI(cfg *Config, currentDir string, database *Database, rpc *BitcoinJSONRPC, port int) {
    if database != nil && port > 0 {
        fmt.Println("\t* Running Iris REST API server")
        // Настраиваем iris
        sessionsConfig := config.DefaultSessions()
        sessionsConfig.DisableSubdomainPersistence = true
        irisConfig := config.Iris{DisableBanner: true, IsDevelopment: true, Sessions: sessionsConfig }
        // Создаем объект API Iris                    
        api := iris.New(irisConfig) 

        fmt.Println("\t* Use CORS middleware")

        // Подключаем CORS
        api.Use(cors.Default())

        // Прокидываем базу данных и rpc клиент внутрь Iris
        api.UseFunc(func (c *iris.Context){            
            c.Set("database", database)
            c.Set("rpc-client", rpc)
            c.Next()
        })

        // Подключаем авторизацию по токену
        api.UseFunc(APIAuthMiddleware)

        // Обработка ошибки 404
        api.OnError(iris.StatusNotFound, func(c *iris.Context){
            c.JSON(iris.StatusNotFound, iris.Map{"error":NewSimpleAPIError(404)})
        })
        // Статические файлы
        if len(cfg.PublicPath) > 0 {
            api.Static("/public",cfg.PublicPath, 1)
        } else { 
            api.Static("/public",currentDir+"/public/", 1)
        }

        // V1 - API
        v1 := api.Party("/api/v1")

        // Инициализируем API Routing        
        InitAPITokens(v1, 1)
        InitAPIWallets(v1, 1)
        InitAPICurrencies(v1, 1)
        InitAPIOffers(v1, 1)
        InitAPIDemands(v1, 1)
        InitAPIDeals(v1, 1)

        // Запускаем доп задач
        StartUpdateCurrencyCurses(database)
        StartValidationTransactions(database, rpc)

        // Запуск сервера
        api.Listen(cfg.Host+":"+strconv.FormatInt(int64(port),10))    

    } else {
        fmt.Println("\t* Error Running Iris REST API server - database is nil") 
    }
}