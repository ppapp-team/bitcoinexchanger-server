package main

import (    
    "strings"    
    "crypto/md5"
    "crypto/sha1"
    "encoding/hex"
)

/* STRING HASH */

// StrToMd5 - MD5 от строки
func StrToMd5(str string) string {
    hasher := md5.New()
    hasher.Write([]byte(str))
    return hex.EncodeToString(hasher.Sum(nil))
}

// StrToSha1 - SHA1 от строки
func StrToSha1(str string) string {
    hasher := sha1.New()
    hasher.Write([]byte(str))
    return hex.EncodeToString(hasher.Sum(nil))
}

/* STRING */

// StrDef - строка относительно базового значения
func StrDef(str string, def string) string {
    if len(strings.TrimSpace(str)) <= 0 {
        return def
    }
    return str
}

// StrRemoveDuplicates - убираем дубликаты из массива строк
func StrRemoveDuplicates(elements []string) []string {
    if len(elements) > 1 {    
        encountered := map[string]bool{}
        result := []string{}
        for v := range elements {
            if encountered[elements[v]] != true {        
                encountered[elements[v]] = true	    
                result = append(result, elements[v])
            }
        }    
        return result
    }
    return elements
}

/* INT */

// IntRemoveDuplicates - убираем дубликаты из массива чисел
func IntRemoveDuplicates(elements []int) []int {
    if len(elements) > 1 {    
        encountered := map[int]bool{}
        result := []int{}
        for v := range elements {
            if encountered[elements[v]] != true {        
                encountered[elements[v]] = true	    
                result = append(result, elements[v])
            }
        }    
        return result
    }
    return elements
}