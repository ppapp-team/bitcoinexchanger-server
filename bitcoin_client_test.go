package main

import (
    "fmt"   
    //"time"  
    "testing"
    "io/ioutil"
    "encoding/json"
    "github.com/btcsuite/btcutil"
    "github.com/btcsuite/btcd/wire"
)

// TestDecodeBitcoinAddress - тестируем декодирование биткоин адреса 
func TestDecodeBitcoinAddress(t *testing.T) {
    address, err := btcutil.DecodeAddress("1BQ9qza7fn9snSCyJQB3ZcN46biBtkt4ee", nil)
    if err != nil {
        t.Error(err.Error())
        return
    }        
    if address.EncodeAddress() != "1BQ9qza7fn9snSCyJQB3ZcN46biBtkt4ee" {
        t.Fail()
        return
    } 
}

func TestGetInfoForValidTransaction(t *testing.T) {
    data, err := ioutil.ReadFile("./config.json")
    if err != nil {
        t.Error(err.Error())
        return
    }
    var config Config
    err = json.Unmarshal(data, &config)
    if err != nil {
        t.Error(err.Error())
        return
    }
    // Инициализация RPC клиента
    var rpcClient BitcoinJSONRPC
    if !rpcClient.Init(config.RPCHost, config.RPCUser, config.RPCPass) { 
        t.Error("Error connection to RPC Bitcoin Server")
        return       
    }
    client := rpcClient.GetClient()
    if client != nil {
        defer client.Disconnect()

        hash, err := wire.NewShaHashFromStr("904d88c6431ecfd23616184e8aad7230329136b37d624095ced4ffe30e8b35e3")
        if err != nil {
            t.Error(err.Error())
            return
        }       
        rawtx, err := client.GetRawTransactionVerbose(hash)
        if err != nil {
            t.Error(err.Error())
            return
        }
        fmt.Println("Confirmations: ", rawtx.Confirmations)
        if rawtx.Confirmations > 0 {               
            return
        }
    }
    t.Fail()
}
