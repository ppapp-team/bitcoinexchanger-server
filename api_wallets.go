package main

import (
    "strings"    
    "github.com/kataras/iris"
    "github.com/btcsuite/btcutil"
    "github.com/legion-zver/utils/unixtime"
)

/* Модели запросов */

// RequestWalletInitV1 - параметры для кошелька
type RequestWalletInitV1 struct {
    AccessKey       string `form:"access_key" json:"access_key"`
    OperationKey    string `form:"operation_key" json:"operation_key"`
}

// RequestWalletSendV1 - операция отправки средств
type RequestWalletSendV1 struct {
    OperationKey    *string  `form:"operation_key" json:"operation_key,omitempty"`
    WalletUUID      *string  `form:"wallet_uuid" json:"wallet_uuid,omitempty"`
    BitcoinAddress  *string  `form:"bitcoin_address" json:"bitcoin_address,omitempty"`
    Amount           float64 `form:"amount" json:"amount"`
    Hash             string  `form:"hash" json:"hash"`
}

// Validation (RequestWalletSendV1) - проверка данных запроса
func (r *RequestWalletSendV1) Validation() bool {
    if r.OperationKey != nil {
        if len(*r.OperationKey) < 32 {
            r.OperationKey = nil
        }       
    }
    if r.WalletUUID != nil {
        if len(*r.WalletUUID) < 36 {
            r.WalletUUID = nil
        }       
    }
    if r.BitcoinAddress != nil {
        if len(*r.BitcoinAddress) < 34 {
            r.BitcoinAddress = nil
        }       
    }
    if (r.WalletUUID != nil || r.BitcoinAddress != nil) && r.Amount > 0.0 && len(r.Hash) > 0 {        
        var tmp string
        if r.OperationKey != nil {
            tmp += *r.OperationKey
        }
        if r.WalletUUID != nil {
            tmp += *r.WalletUUID
        } else {
            tmp += *r.BitcoinAddress
        }
        // fmt.Println("TEST HASH: ",strings.ToLower(StrToSha1(strings.ToLower(tmp)))," == ", strings.ToLower(r.Hash))
        return len(tmp) > 0 && strings.ToLower(StrToSha1(strings.ToLower(tmp))) == strings.ToLower(r.Hash) 
    }
    return false
}

/* Запросы */

// APIWalletInitV1 - инициализация кошелька
func APIWalletInitV1(c *iris.Context) {
    // База данные, RPC, токен доступа
    var db      *Database
    var rpc     *BitcoinJSONRPC
    var token   *Token

    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if c.Get("rpc-client") != nil {
        rpc = c.Get("rpc-client").(*BitcoinJSONRPC)
    }
    if db != nil && rpc != nil {
        orm := db.GetORM(true)//.LogMode(true)
        client := rpc.GetClient()
        if orm != nil && client != nil {
            var request RequestWalletInitV1
            if c.ReadJSON(&request) != nil {
                c.ReadForm(&request)
            }
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)
            }
            if token != nil {
                var user User
                if token.UserID > 0 {
                    orm.Model(token).Related(&user, "User")                                        
                }
                if user.ID > 0 && len(user.UUID) > 24 {
                    var wallet Wallet
                    if user.WalletID > 0 {
                        orm.Where("id = ? AND closed IS NULL", user.WalletID).First(&wallet)
                    }
                    if wallet.ID <= 0 {                    
                        _, err := client.GetAccountAddress(user.UUID)
                        if err != nil {                    
                            err = client.CreateNewAccount(user.UUID)
                            if err != nil {                        
                                c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"account",Сause:err.Error()}})})
                                return
                            }
                        }
                        var bitcoinAddress btcutil.Address
                        // Аккаунт создан, создаем кошелек
                        addressList, err := client.GetAddressesByAccount(user.UUID)
                        if err != nil {
                            c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"account-addresses",Сause:err.Error()}})})
                            return
                        }
                        if len(addressList) <= 0 {
                            bitcoinAddress, err = client.GetAccountAddress(user.UUID)
                            if err != nil {
                                c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"account-address",Сause:err.Error()}})})
                                return
                            }
                        } else {
                            bitcoinAddress = addressList[0]
                        }
                        wallet.UserID         = user.ID
                        wallet.BitcoinAddress = bitcoinAddress.String()                            
                        wallet.Created        = unixtime.Now()
                        wallet.Updated        = wallet.Created
                        wallet.Closed         = nil
                        
                        if len(request.AccessKey) > 32 {
                            request.AccessKey = StrToSha1(strings.ToLower(request.AccessKey))
                            wallet.AccessKey  = &request.AccessKey
                        } else {
                            request.AccessKey = ""
                        }
                        if len(request.OperationKey) > 32 {
                            request.OperationKey = StrToSha1(strings.ToLower(request.OperationKey))
                            wallet.OperationKey  = &request.OperationKey
                        } else {
                            request.OperationKey = ""
                        }
                        orm.Create(&wallet)                    
                        if len(wallet.UUID) < 24 {
                            orm.Where("id = ?", wallet.ID).First(&wallet)
                        }
                        // Присваиваем токену созданный кошелек                        
                        user.WalletID = wallet.ID
                        orm.Save(&user)

                        // Сохранение контрольной суммы
                        wallet.CheckSum = StrToSha1("wallet_crc_"+wallet.BitcoinAddress+"_"+
                                                    request.AccessKey+"_"+request.OperationKey+
                                                    "_uuid_"+wallet.UUID+"_last_updated_"+wallet.Updated.String())
                        orm.Save(&wallet)

                        // Вывод                                    
                        c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":wallet, "meta":iris.Map{"has_operation_key":wallet.OperationKey!=nil, "has_access_key":wallet.AccessKey!=nil}})
                        return                    
                    }
                    // Кошелек уже существует
                    c.JSON(iris.StatusConflict, iris.Map{"error":NewAPIError(409,[]APIErrorCause{APIErrorCause{Target:"wallet",Сause:"Already exist"}})})                
                    return
                }
                // У токена нет пользователя
                c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"user",Сause:"Current token have not information from user, please reinit/refresh token"}})})                
                return    
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    // Сервис недоступен
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

// APIWalletInfoV1 - информация о своем кошельке
func APIWalletInfoV1(c *iris.Context) {
    // База данные, RPC, токен доступа
    var db      *Database
    var rpc     *BitcoinJSONRPC
    var token   *Token

    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if c.Get("rpc-client") != nil {
        rpc = c.Get("rpc-client").(*BitcoinJSONRPC)
    }
    if db != nil && rpc != nil {
        orm := db.GetORM(true)
        client := rpc.GetClient()     
        if orm != nil && client != nil {
            if c.Get("token") != nil {            
                token = c.Get("token").(*Token)
            }
            if token != nil {                
                if token.UserID > 0 {
                    token.User = new(User)
                    orm.Model(token).Related(token.User, "User")
                    if token.User.WalletID > 0 {
                        token.User.Wallet = new(Wallet)
                        orm.Where("user_id = ?", token.User.ID).First(token.User.Wallet)
                    }
                    if token.User.Wallet != nil && token.User.Wallet.ID == token.User.WalletID {                        
                        if token.User.Wallet.AccessKey != nil {
                            // Проверка ключа доступа
                            key := strings.TrimSpace(strings.ToLower(GetArgsValue(c.QueryArgs(), "access_key")))
                            if StrToSha1(key) != strings.ToLower(*token.User.Wallet.AccessKey) {
                                c.JSON(iris.StatusForbidden, iris.Map{"error":NewAPIError(403, []APIErrorCause{APIErrorCause{Сause:"Key is not valid", Target:"access_key"}})})
                                return
                            } 
                        }
                        // Загружаем транзакции
                        orm.Where("wallet_from_id = ? OR wallet_to_id = ?", token.User.WalletID, token.User.WalletID).
                            Preload("WalletFrom").
                            Preload("WalletTo").
                            Limit(20).Order("created desc").Find(&token.User.Wallet.Transactions)
                        
                        // Отсекаем ненужные значения
                        for i := 0; i < len(token.User.Wallet.Transactions); i++ {
                            if token.User.Wallet.Transactions[i].WalletFrom != nil {
                                token.User.Wallet.Transactions[i].WalletFromUUID = &token.User.Wallet.Transactions[i].WalletFrom.UUID
                                token.User.Wallet.Transactions[i].WalletFrom = nil
                            }
                            if token.User.Wallet.Transactions[i].WalletTo != nil {
                                token.User.Wallet.Transactions[i].WalletToUUID = &token.User.Wallet.Transactions[i].WalletTo.UUID
                                token.User.Wallet.Transactions[i].WalletTo = nil
                            }
                        } 

                        // Выводим на экран
                        c.JSON(iris.StatusOK, iris.Map{"error":nil, "result":token.User.Wallet, "meta":iris.Map{"has_operation_key":token.User.Wallet.OperationKey!=nil, "has_access_key":token.User.Wallet.AccessKey!=nil}})
                        return
                    }
                }
                // Ошибка - кошелек не найден
                c.JSON(iris.StatusNotFound, iris.Map{"error":NewAPIError(401, []APIErrorCause{APIErrorCause{Сause:"Not found wallet, please init first", Target:"wallet"}})})
                return
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    // Сервис недоступен
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})  
} 

// APIWalletBalanceV1 - получение баланса кошелька
func APIWalletBalanceV1(c *iris.Context) {
    // База данные, RPC, токен доступа
    var db      *Database
    var rpc     *BitcoinJSONRPC
    var token   *Token
    
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if c.Get("rpc-client") != nil {
        rpc = c.Get("rpc-client").(*BitcoinJSONRPC)
    }    
    if db != nil && rpc != nil {
        orm := db.GetORM(true)   
        client := rpc.GetClient()     
        if orm != nil && client != nil {
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)
            }
            if token != nil {              
                if token.UserID > 0 {
                    token.User = new(User)
                    orm.Model(token).Related(token.User, "User")
                    if token.User.WalletID > 0 {
                        token.User.Wallet = new(Wallet)
                        orm.Model(token.User).Related(token.User.Wallet, "Wallet")
                    }
                    if len(token.User.UUID) > 24 && token.User.Wallet != nil && token.User.Wallet.ID == token.User.WalletID {
                        if token.User.Wallet.AccessKey != nil {
                            // Проверка ключа доступа
                            key := strings.TrimSpace(strings.ToLower(GetArgsValue(c.QueryArgs(), "access_key")))
                            if StrToSha1(key) != strings.ToLower(*token.User.Wallet.AccessKey) {
                                c.JSON(iris.StatusForbidden, iris.Map{"error":NewAPIError(403, []APIErrorCause{APIErrorCause{Сause:"Key is not valid", Target:"access_key"}})})
                                return
                            }                            
                        }
                        // Наш аккаунт                        
                        amount, err := client.GetBalance(token.User.UUID)
                        if err != nil {
                            c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"balance",Сause:err.Error()}})})
                        } else {
                            c.JSON(iris.StatusOK, iris.Map{"error":nil, "result":amount.ToBTC(), "meta":iris.Map{"type":"BTC"}})
                        }                        
                        return
                    }
                }
                // Ошибка - кошелек не найден
                c.JSON(iris.StatusNotFound, iris.Map{"error":NewAPIError(401, []APIErrorCause{APIErrorCause{Сause:"Not found wallet, please init first", Target:"wallet"}})})
                return
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    // Сервис недоступен
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

// APIWalletSendV1 - отправка биткоинов на адрес или кошелек
func APIWalletSendV1(c *iris.Context) {
    // База данные, RPC, токен доступа
    var db      *Database
    var rpc     *BitcoinJSONRPC
    var token   *Token

    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if c.Get("rpc-client") != nil {
        rpc = c.Get("rpc-client").(*BitcoinJSONRPC)
    }    
    if db != nil && rpc != nil {
        orm := db.GetORM(true)   
        client := rpc.GetClient()     
        if orm != nil && client != nil {
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)
            }
            if token != nil {              
                if token.UserID > 0 {
                    token.User = new(User)
                    orm.Model(token).Related(token.User, "User")
                    if token.User.WalletID > 0 {
                        token.User.Wallet = new(Wallet)
                        orm.Model(token.User).Related(token.User.Wallet, "Wallet")
                    }
                    if len(token.User.UUID) > 24 && token.User.Wallet != nil && token.User.Wallet.ID == token.User.WalletID {
                        var request RequestWalletSendV1 
                        if err := c.ReadJSON(&request); err != nil {
                            c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"request",Сause:err.Error()}})})
                            return
                        }
                        if request.Validation() {
                            if token.User.Wallet.OperationKey != nil {
                                // Проверка кода на операции
                                if request.OperationKey != nil && StrToSha1(strings.ToLower(*request.OperationKey)) != strings.ToLower(*token.User.Wallet.OperationKey) {
                                    c.JSON(iris.StatusForbidden, iris.Map{"error":NewAPIError(403, []APIErrorCause{APIErrorCause{Сause:"Key is not valid", Target:"operation_key"}})})
                                    return
                                }
                            }
                            // Все прошло успешно, проверяем баланс
                            amount, err := client.GetBalance(token.User.UUID)
                            if err != nil {
                                c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"balance",Сause:err.Error()}})})
                                return
                            }
                            if amount.ToBTC() >= request.Amount {
                                // Проверяем какая будет сделка
                                if request.WalletUUID != nil {
                                    // Получаем кошелек 
                                    var toWallet Wallet
                                    orm.Where("uuid = ? and closed IS NULL", request.WalletUUID).First(&toWallet)
                                    if toWallet.ID > 0 && len(toWallet.BitcoinAddress) >= 34 {
                                        address, err := btcutil.DecodeAddress(toWallet.BitcoinAddress, nil)
                                        if err != nil {
                                            c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"request.wallet.bitcoin_address",Сause:err.Error()}})})
                                            return
                                        }
                                        amount, err := btcutil.NewAmount(request.Amount)
                                        if err != nil {
                                            c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"request.amount",Сause:err.Error()}})})
                                            return
                                        }
                                        tx, err := client.SendFrom(token.User.UUID, address, amount)
                                        if err != nil {
                                            c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"send_from",Сause:err.Error()}})})
                                            return
                                        }
                                        // Создаем транзакцию и выводим ее
                                        var transaction = WalletTransaction{WalletFromID:token.User.Wallet.ID,
                                                                            WalletToID: toWallet.ID,
                                                                            BitcoinTransaction: tx.String(), 
                                                                            Amount: request.Amount, 
                                                                            Created: unixtime.Now()}

                                        orm.Create(&transaction)

                                        // Модифицируем для корректного вывода
                                        transaction.WalletFromUUID = &token.User.Wallet.UUID
                                        transaction.WalletToUUID = &toWallet.UUID

                                        // Выводим
                                        c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":transaction})
                                        return
                                    }
                                    c.JSON(iris.StatusForbidden, iris.Map{"error":NewAPIError(404,[]APIErrorCause{APIErrorCause{Target:"request.wallet",Сause:"Not found"}})})
                                    return
                                }
                                // Отправляем средтсва на BitcoinAddress
                                address, err := btcutil.DecodeAddress(*request.BitcoinAddress, nil)
                                if err != nil {
                                    c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"request.bitcoin_address",Сause:err.Error()}})})
                                    return
                                }
                                amount, err := btcutil.NewAmount(request.Amount)
                                if err != nil {
                                    c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"request.amount",Сause:err.Error()}})})
                                    return
                                }
                                tx, err := client.SendFrom(token.User.UUID, address, amount)
                                if err != nil {
                                    c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"send_from",Сause:err.Error()}})})
                                    return
                                }
                                // Создаем транзакцию и выводим ее
                                var transaction = WalletTransaction{WalletFromID:token.User.Wallet.ID,
                                                                    BitcoinAddress: request.BitcoinAddress,
                                                                    BitcoinTransaction: tx.String(), 
                                                                    Amount: request.Amount, 
                                                                    Created: unixtime.Now()}

                                orm.Create(&transaction)

                                // Модифицируем для корректного вывода
                                transaction.WalletFromUUID = &token.User.Wallet.UUID
                                
                                // Выводим
                                c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":transaction})
                                return
                            }
                            c.JSON(iris.StatusForbidden, iris.Map{"error":NewAPIError(403,[]APIErrorCause{APIErrorCause{Target:"wallet.balance",Сause:"There is no required amount of Bitcoins"}})})
                            return                            
                        }
                        // Не прошли проверку валидации, запрет
                        c.JSON(iris.StatusForbidden, iris.Map{"error":NewSimpleAPIError(29)})
                        return
                    }
                }                
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    // Сервис недоступен
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

/* Инициализация */

// InitAPIWallets - инициализация API для токенов доступа
func InitAPIWallets(api iris.MuxAPI, version int) {
    // Для первой версии
    wallets := api.Party("/wallet")
    if version == 1 {
        /**
        * @api {post} /v1/wallet/init Инициализация (создание) кошелька
        * @apiName InitWallet
        * @apiGroup Wallet
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {String} [access_key] Ключ доступа к кошельку (sha1)
        * @apiParam {String} [operation_key] Ключ для совершения операций с кошельком (sha1)        
        *
        * @apiSuccess (201) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (201) {Object} result Объект кошелька (model:Wallet)
        * @apiSuccess (201) {String} result.uuid Уникальный UUID для кошелька
        * @apiSuccess (201) {String} result.bitcoin_address Уникальный Bitcoin адрес
        * @apiSuccess (201) {Object[]} transactions Пустой список транзакций
        * @apiSuccess (201) {String} result.created Дата создания кошелька
        * @apiSuccess (201) {String} result.updated Дата изменения параметров кошелька
        * @apiSuccess (201) {Object} meta Метаданные кошелька
        * @apiSuccess (201) {Bool} meta.has_access_key Назначен ли ключ доступа к кошельку
        * @apiSuccess (201) {Bool} meta.has_operation_key Назначен ли операционный ключ к кошельку         
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 29,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 409 CONFLICT
        *     {
        *         "error": {
        *             "code": 409,
        *             "causes": [{
        *                 "target": "wallet",
        *                 "cause": "Already exist"
        *             }],
        *             "description": "Conflict"
        *         }
        *     }
        */
        wallets.Post("/init",   APIWalletInitV1)

        /**
        * @api {get} /v1/wallet/info Получение информации о своем кошельке
        * @apiName GetWalletInfo
        * @apiGroup Wallet
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {String} [access_key] Ключ доступа к кошельку (sha1)                
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object} result Объект кошелька (model:Wallet)
        * @apiSuccess (200) {String} result.uuid Уникальный UUID для кошелька
        * @apiSuccess (200) {String} result.bitcoin_address Уникальный Bitcoin адрес
        * @apiSuccess (200) {Object[]} transactions Список последних 20 транзакций для кошелька (model:WalletTransaction)
        * @apiSuccess (200) {Object} transactions.wallet_from_uuid UUID кошелека с которого перевели биткоины
        * @apiSuccess (200) {Object} transactions.wallet_to_uuid UUID кошелека на который перевели биткоины
        * @apiSuccess (200) {String} transactions.bitcoin_address Bitcoin адрес, будет не равен null только в том случае если транзакция осущесвлялась не на кошелек сервиса, а на конкретный адрес
        * @apiSuccess (200) {String} transactions.bitcoin_transaction Адрес Bitcoin транзакции
        * @apiSuccess (200) {Number} transactions.amount Количество переводимых средств
        * @apiSuccess (200) {String} transactions.created Дата создания транзакции
        * @apiSuccess (200) {String} transactions.сonfirmed=null Дата подтверждения транзакции
        * @apiSuccess (200) {String} transactions.canceled=null Дата отмены транзакции
        * @apiSuccess (200) {String} result.created Дата создания кошелька
        * @apiSuccess (200) {String} result.updated Дата изменения параметров кошелька        
        * @apiSuccess (200) {Object} meta Метаданные кошелька
        * @apiSuccess (200) {Bool} meta.has_access_key Назначен ли ключ доступа к кошельку
        * @apiSuccess (200) {Bool} meta.has_operation_key Назначен ли операционный ключ к кошельку         
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "meta": {
        *            "has_access_key": false,
        *            "has_operation_key": false
        *        },
        *        "result": {
        *            "bitcoin_address": "miEsRU1D77R4MhtAf1RruBVMEuMJeosiJy",
        *            "created": 1469492403,
        *            "uuid": "eec91281-52b5-11e6-9963-5254003dd900",
        *            "transactions": [{
        *                "wallet_from_uuid": "eec91281-52b5-11e6-9963-5254003dd900",
        *                "bitcoin_address": "mocAiNBUTm2kB5TG2turQDvRXaweLsu21k",
        *                "bitcoin_transaction": "7af728854b7b1dbd778741f1dfe874fcb1d657da52275d3d9928ac177300194e",
        *                "amount": 5.12,
        *                "created": 1469538767,
        *                "сonfirmed": 1469538802
        *            }, {
        *                "wallet_from_uuid": "eec91281-52b5-11e6-9963-5254003dd900",
        *                "bitcoin_address": "mocAiNBUTm2kB5TG2turQDvRXaweLsu21k",
        *                "bitcoin_transaction": "904d88c6431ecfd23616184e8aad7230329136b37d624095ced4ffe30e8b35e3",
        *                "amount": 5.12,
        *                "created": 1469537381,
        *                "сonfirmed": 1469538020
        *            }],
        *            "updated": 1469492403
        *        }
        *    }        
        */
        wallets.Get("/info",    APIWalletInfoV1)

        /**
        * @api {get} /v1/wallet/balance Получение баланса кошелька
        * @apiName GetWalletBallance
        * @apiGroup Wallet
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {String} [access_key] Ключ доступа к кошельку (sha1)                
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Number} result баланс (0.0)        
        * @apiSuccess (200) {Object} meta Метаданные результата
        * @apiSuccess (200) {Bool} meta.type=BTC Тип результата, тип биткоинов                
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "meta": {
        *            "type": "BTC"
        *        },
        *        "result": 40.8552802
        *    }        
        */
        wallets.Get("/balance", APIWalletBalanceV1)

        /**
        * @api {post} /v1/wallet/send Отправить средства на кошелек или адрес
        * @apiName WalletSendBitcoins
        * @apiGroup Wallet
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {String} [operation_key] Операционный ключ к кошельку (sha1)                
        *
        * @apiSuccess (201) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (201) {Object} result Объект транзакций (model: WalletTransaction)        
        * @apiSuccess (201) {Object} result.wallet_from_uuid UUID кошелека с которого перевели биткоины
        * @apiSuccess (201) {Object} result.wallet_to_uuid UUID кошелека на который перевели биткоины
        * @apiSuccess (201) {String} result.bitcoin_address Bitcoin адрес, будет не равен null только в том случае если транзакция осущесвлялась не на кошелек сервиса, а на конкретный адрес
        * @apiSuccess (201) {String} result.bitcoin_transaction Адрес Bitcoin транзакции
        * @apiSuccess (201) {Number} result.amount Количество переводимых средств
        * @apiSuccess (201) {String} result.created Дата создания транзакции
        * @apiSuccess (201) {String} result.сonfirmed=null Дата подтверждения транзакции
        * @apiSuccess (201) {String} result.canceled=null Дата отмены транзакции (в случае возникновения ошибок)
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 201 CREATE        
        *    {
        *        "error": null,
        *        "result": {
        *            "wallet_from_uuid": "eec91281-52b5-11e6-9963-5254003dd900",
        *            "bitcoin_address": "mocAiNBUTm2kB5TG2turQDvRXaweLsu21k",
        *            "bitcoin_transaction": "1d404e77dae28889ed9627538f94cd0484d26a105074627f210bff5655e48153",
        *            "amount": 5.12,
        *            "created": 1470206451
        *        }
        *    }        
        */
        wallets.Post("/send",   APIWalletSendV1)
    }
}