package main

import (
    "github.com/btcsuite/btcrpcclient"
)

// BitcoinClientRPC - интерфейс для обеспечения работы с Bitcoin RPC сервером
type BitcoinClientRPC interface {
    Init(host string, user string, pass string) bool
    GetNewClient() *btcrpcclient.Client
    GetClient() *btcrpcclient.Client
}

// BitcoinJSONRPC - объект для обеспечения работы клиента RPC
type BitcoinJSONRPC struct {
    BaseClient *btcrpcclient.Client
    Host string
    User string
    Pass string
}

// Init - иницализация
func (rpc *BitcoinJSONRPC) Init(host string, user string, pass string) bool {
    rpc.Host = host
    rpc.User = user
    rpc.Pass = pass
    return rpc.GetClient() != nil
}

// GetNewClient - новое подключение к RPC
func (rpc *BitcoinJSONRPC) GetNewClient() *btcrpcclient.Client {
    conf := btcrpcclient.ConnConfig{
		Host:         rpc.Host,
		User:         rpc.User,
		Pass:         rpc.Pass,
		HTTPPostMode: true, // Bitcoin core only supports HTTP POST mode
		DisableTLS:   true, // Bitcoin core does not provide TLS by default
	}    
    client, err := btcrpcclient.New(&conf, nil)
    if err != nil {
        return nil
    }    
    err = client.Ping()
    if err != nil {
        defer client.Shutdown()
        client = nil
    } 
    return client
}

// GetClient - базовое подключение
func (rpc *BitcoinJSONRPC) GetClient() *btcrpcclient.Client {
    if rpc.BaseClient != nil {
        err := rpc.BaseClient.Ping()
        if err == nil {
            return rpc.BaseClient
        }        
        rpc.BaseClient.Shutdown()
        rpc.BaseClient = nil
    }
    rpc.BaseClient = rpc.GetNewClient()
    return rpc.BaseClient
}