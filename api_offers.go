package main

import (    
    //"fmt"
    "strings"
    "strconv"    
    "github.com/jinzhu/gorm"
    "github.com/kataras/iris"
    "github.com/valyala/fasthttp"
    "github.com/legion-zver/utils/unixtime"
)

/* Модели запросов */

/* Для запросов */

// SetSortOffersFromURL - установить сортировку из параметров URL в текущий GORM запрсов 
func SetSortOffersFromURL(orm *gorm.DB, urlParams *fasthttp.Args) *gorm.DB {
    if orm != nil && urlParams != nil {
        if urlParams.Has("order[id]") {
            orm = orm.Order("id "+GetOrderDirect(urlParams.GetUfloatOrZero("order[id]") > 0))
        }
        if urlParams.Has("order[currency_id]") {
            orm = orm.Order("currency_id "+GetOrderDirect(urlParams.GetUfloatOrZero("order[currency_id]") > 0))
        }
        if urlParams.Has("order[created]") {
            orm = orm.Order("created "+GetOrderDirect(urlParams.GetUfloatOrZero("order[created]") > 0))
        }                
        if urlParams.Has("order[updated]") {
            orm = orm.Order("updated "+GetOrderDirect(urlParams.GetUfloatOrZero("order[updated]") > 0))
        }
        if urlParams.Has("order[min_amount]") {
            orm = orm.Order("min_amount "+GetOrderDirect(urlParams.GetUfloatOrZero("order[min_amount]") > 0))
        }
        if urlParams.Has("order[amount]") {
            orm = orm.Order("amount "+GetOrderDirect(urlParams.GetUfloatOrZero("order[amount]") > 0))
        }
        if urlParams.Has("order[course]") {
            orm = orm.Order("course "+GetOrderDirect(urlParams.GetUfloatOrZero("order[course]") > 0))
        }
    }
    return orm
}

/* Запросы */

// APIOfferCreateV1 - создание предложения
func APIOfferCreateV1(c *iris.Context) {
    var db      *Database
    var token   *Token
    if c.Get("database") != nil {    
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)        
        if orm != nil {
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)
            }
            if token != nil && token.UserID > 0 {
                var offer Offer
                err := c.ReadJSON(&offer)
                if err != nil {
                    c.JSON(iris.StatusBadRequest, iris.Map{"error":NewAPIError(400,[]APIErrorCause{APIErrorCause{Target:"offer",Сause:err.Error()}})})
                    return
                }
                // Небольшие правки модели предложения
                offer.ID      = 0                
                offer.UserID  = token.UserID
                offer.User    = nil                
                offer.Created = unixtime.Now()
                offer.Updated = offer.Created

                // Проверка валюты 
                if offer.Currency != nil {
                    if offer.Currency.ID > 0 && offer.CurrencyID != offer.Currency.ID {
                        offer.CurrencyID = offer.Currency.ID
                    }
                    offer.Currency = nil
                }
                var templateIDs []int
                if offer.ExchangeMethods != nil {
                    for i := 0; i < len(offer.ExchangeMethods); i++ {
                        // Проверка для правильного создания
                        offer.ExchangeMethods[i].OfferID = 0                        
                        offer.ExchangeMethods[i].Offer = nil    
                        offer.ExchangeMethods[i].Created = offer.Created
                        offer.ExchangeMethods[i].Updated = offer.Created
                        // Проверка шаблонов
                        if offer.ExchangeMethods[i].Template != nil && offer.ExchangeMethods[i].TemplateID != offer.ExchangeMethods[i].Template.ID {
                            offer.ExchangeMethods[i].TemplateID = offer.ExchangeMethods[i].Template.ID
                        }
                        // Составление списка IDs шаблонов
                        templateIDs = append(templateIDs, offer.ExchangeMethods[i].TemplateID)             
                    }
                }
                // Валидация структуры предложения
                err = offer.StructValidation()
                if err != nil {
                    c.JSON(iris.StatusConflict, iris.Map{"error":NewAPIError(409,[]APIErrorCause{APIErrorCause{Target:"offer:struct",Сause:err.Error()}})})
                    return
                }
                // Получение всех шаблонов
                var templates []TemplateExchangeMethod
                orm.Where("id in (?)", templateIDs).Find(&templates)
                if len(templates) <= 0 {
                    c.JSON(iris.StatusNotFound, iris.Map{"error":NewAPIError(404,[]APIErrorCause{APIErrorCause{Target:"offer:methods:templates",Сause:"Empty"}})})
                    return
                }
                // Валидация структуры методов обмена с шаблонами
                for _, template := range templates {                    
                    for i := 0; i < len(offer.ExchangeMethods); i++ {                        
                        if offer.ExchangeMethods[i].TemplateID == template.ID {
                            if err = template.MethodValidation(&offer.ExchangeMethods[i]); err != nil {
                                c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"offer:methods:templates:"+strconv.FormatInt(int64(template.ID),10),Сause:err.Error()}})})
                                return
                            }
                            break
                        }
                    }
                }                      
                // Если все прошло удачно - создание предложения                
                orm.Create(&offer)
                if orm.Error != nil {
                    c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewAPIError(500,[]APIErrorCause{APIErrorCause{Target:"offer:sql-create",Сause:orm.Error.Error()}})})
                    return
                }                
                // Подготовка к выводу
                offer.Currency = new(Currency)
                // Загрузка созданных методов перевода
                orm.Where("offer_id = ?", offer.ID).Preload("Template").Find(&offer.ExchangeMethods)
                orm.Where("id = ?", offer.CurrencyID).Find(offer.Currency)

                // Вывод                                    
                c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":offer})
                return
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

// APIOfferSearchV1 - поиск предложений
func APIOfferSearchV1(c *iris.Context) {
    var db      *Database
    var token   *Token
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)        
        if orm != nil {
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)
            }
            if token != nil && token.UserID > 0 {
                urlParams := c.QueryArgs()
                var offers []Offer
                var limit, offset = 20, 0
                // Лимиты и смещение
                if urlParams.Has("limit") {
                    limit = urlParams.GetUintOrZero("limit")
                    if limit <= 0 {
                        limit = 20
                    }
                }
                if urlParams.Has("offset") {
                    offset = urlParams.GetUintOrZero("offset")                    
                }
                // Обработка фильтров
                ids := GetIntArrayFromFilter("id", urlParams)
                if len(ids) > 0 {
                    orm = orm.Where(ids)
                }
                currencyIDs := GetIntArrayFromFilter("currency_id", urlParams)
                if len(currencyIDs) > 0 {
                    orm = orm.Where("currency_id in (?)", currencyIDs)
                }
                if urlParams.Has("filter[type]") {                    
                    orm = orm.Where("type = ?", strings.ToLower(string(urlParams.Peek("filter[type]"))))
                }
                // MIN & MAX
                if urlParams.Has("min[id]") {
                    orm = orm.Where("id >= ?", urlParams.GetUfloatOrZero("min[id]"))
                }
                if urlParams.Has("max[id]") {
                    orm = orm.Where("id <= ?", urlParams.GetUfloatOrZero("max[id]"))
                }
                if urlParams.Has("min[amount]") {
                    orm = orm.Where("amount >= ?", urlParams.GetUfloatOrZero("min[amount]"))
                }
                if urlParams.Has("max[amount]") {
                    orm = orm.Where("amount <= ?", urlParams.GetUfloatOrZero("max[amount]"))
                }
                if urlParams.Has("min[min_amount]") {
                    orm = orm.Where("min_amount >= ?", urlParams.GetUfloatOrZero("min[min_amount]"))
                }
                if urlParams.Has("max[min_amount]") {
                    orm = orm.Where("min_amount <= ?", urlParams.GetUfloatOrZero("max[min_amount]"))
                }
                if urlParams.Has("min[course]") {
                    orm = orm.Where("course >= ?", urlParams.GetUfloatOrZero("min[course]"))
                }
                if urlParams.Has("max[course]") {
                    orm = orm.Where("course <= ?", urlParams.GetUfloatOrZero("max[course]"))
                }
                // Сортировки ORDERS                
                orm = SetSortOffersFromURL(orm, urlParams)
                // + Доб условия в будущем
                orm.Where("user_id != ? AND closed IS NULL ANS validated IS NOT NULL", token.UserID).
                    Limit(limit).Offset(offset).Order("validated",true).
                    Preload("Currency").Preload("User").
                    Preload("ExchangeMethods.Template").                    
                    Find(&offers)

                // Вывод                                    
                c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":offers})
                return
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

// APIOfferGetListV1 - получить список своих предложений
func APIOfferGetListV1(c *iris.Context) {
    var db    *Database
    var token *Token
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)        
        if orm != nil {
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)
            }
            if token != nil && token.UserID > 0 {
                urlParams := c.QueryArgs()
                var offers []Offer
                var limit, offset = 20, 0
                // Лимиты и смещение
                if urlParams.Has("limit") {
                    limit = urlParams.GetUintOrZero("limit")
                    if limit <= 0 {
                        limit = 20
                    }
                }
                if urlParams.Has("offset") {
                    offset = urlParams.GetUintOrZero("offset")                    
                }
                // Фильтрация по ID
                ids := GetIntArrayFromFilter("id", urlParams)
                if len(ids) > 0 {
                    orm = orm.Where(ids)
                }
                // MIN & MAX
                if urlParams.Has("min[id]") {
                    orm = orm.Where("id >= ?", urlParams.GetUfloatOrZero("min[id]"))
                }
                if urlParams.Has("max[id]") {
                    orm = orm.Where("id <= ?", urlParams.GetUfloatOrZero("max[id]"))
                }
                // Сортировки ORDERS                
                orm = SetSortOffersFromURL(orm, urlParams)

                orm.Where("user_id = ?", token.UserID).
                    Limit(limit).Offset(offset).Order("validated",false).
                    Preload("Currency").
                    Preload("ExchangeMethods.Template").                    
                    Find(&offers)

                // Вывод                                    
                c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":offers})
                return
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

/* Инициализация API */

// InitAPIOffers - инициализация API для предложений
func InitAPIOffers(api iris.MuxAPI, version int) {
    offers := api.Party("/offers")
    if version == 1 {
        /**
        * @api {post} /v1/offers Создание предложения о купле/продаже биткоинов
        * @apiName CreateOffer
        * @apiGroup Offers
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {String="sell","buy"} type Тип предложения
        * @apiParam {String} description Описание предложения (Сопроводительный текст или условия)
        * @apiParam {Number} amount Cумма сделки в BTC
        * @apiParam {Number} min_amount Минимальная сумма сделки в BTC
        * @apiParam {Number} currency_id Идентификатор фиатной валюты
        * @apiParam {Number} course Предпочтительный курс фиатной валюты (сумма за 1 BTC)
        * @apiParam {Object[]} exchange_methods Доступные методы обмена фиатной валюты на BTC
        * @apiParam {Number} exchange_methods.template_id Идентификатор шаблона обмена
        * @apiParam {String} exchange_methods.description Описание / Сопроводительный текст / Условия
        * @apiParam {Object} exchange_methods.requisites JSON параметров обмена в соответствии с выбранным шаблоном
        *
        * @apiSuccess (201) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (201) {Object} result Объект предложения (model: Offer)
        * @apiSuccess (201) {Number} result.id Идентификатор предложения        
        * @apiSuccess (201) {String="sell","buy"} result.type Тип предложения
        * @apiSuccess (201) {String} result.description Описание предложения (Сопроводительный текст или условия)
        * @apiSuccess (201) {Number} result.amount Cумма сделки в BTC
        * @apiSuccess (201) {Number} result.min_amount Минимальная сумма сделки в BTC
        * @apiSuccess (201) {Number} result.currency_id Идентификатор фиатной валюты
        * @apiSuccess (201) {Object} result.currency Объект фиатной валюты (model: Currency)
        * @apiSuccess (201) {Number} result.currency.id Идентификатор фиатной валюты
        * @apiSuccess (201) {String} result.currency.code Трех символьный код фиатной валюты (RUS, EUR...)
        * @apiSuccess (201) {String} result.currency.symbol Unicode символ фиатной валюты ($...)
        * @apiSuccess (201) {Number} result.currency.buy_course Текущий курс BTC на покупку
        * @apiSuccess (201) {Number} result.currency.sell_course Текущий курс BTC на продажу
        * @apiSuccess (201) {Number} result.course Предпочтительный курс фиатной валюты (сумма за 1 BTC)
        * @apiSuccess (201) {Object[]} result.exchange_methods Доступные методы обмена фиатной валюты на BTC (model:ExchangeMethod)
        * @apiSuccess (201) {Number} result.exchange_methods.id Идентификатор метода обмена
        * @apiSuccess (201) {Number} result.exchange_methods.offer_id Идентификатор предложения
        * @apiSuccess (201) {Number} result.exchange_methods.template_id Идентификатор шаблона обмена
        * @apiSuccess (201) {Object} result.exchange_methods.template Объект шаблона обмена (model: TemplateExchangeMethod)
        * @apiSuccess (201) {Number} result.exchange_methods.template.id Идентификатор шаблона обмена
        * @apiSuccess (201) {String} result.exchange_methods.template.name Название шаблона обмена
        * @apiSuccess (201) {Bool} result.exchange_methods.template.online Есть ли возможность онлайн перевода
        * @apiSuccess (201) {String} result.exchange_methods.template.url Ссылка на сайт для осуществления перевода
        * @apiSuccess (201) {Object} result.exchange_methods.template.validator JSON с RegExp проверками параметров обмена)
        * @apiSuccess (201) {String} result.exchange_methods.description Описание / Сопроводительный текст / Условия
        * @apiSuccess (201) {Object} result.exchange_methods.requisites JSON параметров обмена в соответствии с выбранным шаблоном
        * @apiSuccess (201) {String} result.created Дата создания предложения
        * @apiSuccess (201) {String} result.updated Дата изменения предложения
        * @apiSuccess (201) {String} [result.closed] Дата удаления / отмены предложения        
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 201 CREATE
        *    {
        *        "error": null,
        *        "result": {
        *            "id": 3,
        *            "type": "sell",
        *            "description": "Продам биткоины, срочно! Только за рубли",
        *            "amount": 9,
        *            "min_amount": 1,
        *            "currency": {
        *                "id": 1,
        *                "code": "RUB",
        *                "symbol": "₽",
        *                "buy_course": 35806.39,
        *                "sell_course": 35863.06
        *            },
        *            "currency_id": 1,
        *            "exchange_methods": [{
        *                "id": 3,
        *                "template": {
        *                    "id": 1,
        *                    "name": "Перевод на карту Сбербанка",
        *                    "online": false,
        *                    "url": "http://www.sberbank.ru/ru/person/paymentsandremittances/remittance/in#intbeznal",
        *                    "validator": {
        *                        "Card Number": "^(?:4[0-9]{12}(?:[0-9]{3})?|(?:5[1-5][0-9]{2}))$"
        *                    }
        *                },
        *                "template_id": 1,
        *                "offer_id": 3,
        *                "requisites": {
        *                    "Card Number": "4111111111111111"
        *                },
        *                "description": "На карту сбербанка, только с карты на карту!!!",
        *                "created": 1470223831,
        *                "updated": 1470223831
        *            }],
        *            "course": 45000,
        *            "created": 1470223831,
        *            "updated": 1470223831
        *        }
        *    }        
        */
        offers.Post("", APIOfferCreateV1)

        /**
        * @api {get} /v1/offers Получение списка своих предложений о купле/продаже биткоинов
        * @apiName GetListMyOffer
        * @apiGroup Offers
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {Number} [limit=20] Количество запрашиваемых предложений
        * @apiParam {Number} [offset=0] Смещение запрашиваемых предложений
        * @apiParam {Number} [min[id]] Минимальное значение ID преложения (Условие: id >= min[id])
        * @apiParam {Number} [max[id]] Максимальное значение ID преложения (Условие: id <= max[id])
        * @apiParam {Number[]} [filter[id]] Фильтр по идентификаторам (Условие: id in (filter[id]))        
        * @apiParam {Number} [order[id]] Направление сортировки по ID
        * @apiParam {Number} [order[currency_id]] Направление сортировки по идентификатору фиатной валюты
        * @apiParam {Number} [order[created]] Направление сортировки по дате создания предложений
        * @apiParam {Number} [order[updated]] Направление сортировки по дате обновления предложений (лучше использовать вместо order[created])
        * @apiParam {Number} [order[amount]] Направление сортировки по сумме сделки
        * @apiParam {Number} [order[min_amount]] Направление сортировки по минимальной сумме сделки
        * @apiParam {Number} [order[course]] Направление сортировки по сумме заявленного курса BTC        
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object[]} result Объекты предложений (model: Offer)
        * @apiSuccess (200) {Number} result.id Идентификатор предложения        
        * @apiSuccess (200) {String="sell","buy"} result.type Тип предложения
        * @apiSuccess (200) {String} result.description Описание предложения (Сопроводительный текст или условия)
        * @apiSuccess (200) {Number} result.amount Cумма сделки в BTC
        * @apiSuccess (200) {Number} result.min_amount Минимальная сумма сделки в BTC
        * @apiSuccess (200) {Number} result.currency_id Идентификатор фиатной валюты
        * @apiSuccess (200) {Object} result.currency Объект фиатной валюты (model: Currency)
        * @apiSuccess (200) {Number} result.currency.id Идентификатор фиатной валюты
        * @apiSuccess (200) {String} result.currency.code Трех символьный код фиатной валюты (RUS, EUR...)
        * @apiSuccess (200) {String} result.currency.symbol Unicode символ фиатной валюты ($...)
        * @apiSuccess (200) {Number} result.currency.buy_course Текущий курс BTC на покупку
        * @apiSuccess (200) {Number} result.currency.sell_course Текущий курс BTC на продажу
        * @apiSuccess (200) {Number} result.course Предпочтительный курс фиатной валюты (сумма за 1 BTC)
        * @apiSuccess (200) {Object[]} result.exchange_methods Доступные методы обмена фиатной валюты на BTC (model:ExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_methods.id Идентификатор метода обмена
        * @apiSuccess (200) {Number} result.exchange_methods.offer_id Идентификатор предложения
        * @apiSuccess (200) {Number} result.exchange_methods.template_id Идентификатор шаблона обмена
        * @apiSuccess (200) {Object} result.exchange_methods.template Объект шаблона обмена (model: TemplateExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_methods.template.id Идентификатор шаблона обмена
        * @apiSuccess (200) {String} result.exchange_methods.template.name Название шаблона обмена
        * @apiSuccess (200) {Bool} result.exchange_methods.template.online Есть ли возможность онлайн перевода
        * @apiSuccess (200) {String} result.exchange_methods.template.url Ссылка на сайт для осуществления перевода
        * @apiSuccess (200) {Object} result.exchange_methods.template.validator JSON с RegExp проверками параметров обмена)
        * @apiSuccess (200) {String} result.exchange_methods.description Описание / Сопроводительный текст / Условия
        * @apiSuccess (200) {Object} result.exchange_methods.requisites JSON параметров обмена в соответствии с выбранным шаблоном
        * @apiSuccess (200) {String} result.created Дата создания предложения
        * @apiSuccess (200) {String} result.updated Дата изменения предложения
        * @apiSuccess (200) {String} [result.validated] Дата допуска предложения к платформе
        * @apiSuccess (200) {String} [result.closed] Дата удаления / отмены предложения
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "result": [{
        *            "id": 1,
        *            "type": "sell",
        *            "description": "Продам биткоины, срочно! Только за рубли",
        *            "amount": 0,
        *            "min_amount": 0,
        *            "currency": {
        *                "id": 1,
        *                "code": "RUB",
        *                "symbol": "₽",
        *                "buy_course": 35734.39,
        *                "sell_course": 35788.39
        *            },
        *            "currency_id": 1,
        *            "exchange_methods": [{
        *                "id": 1,
        *                "template": {
        *                    "id": 1,
        *                    "name": "Перевод на карту Сбербанка",
        *                    "online": false,
        *                    "url": "http://www.sberbank.ru/ru/person/paymentsandremittances/remittance/in#intbeznal",
        *                    "validator": {
        *                        "Card Number": "^(?:4[0-9]{12}(?:[0-9]{3})?|(?:5[1-5][0-9]{2}))$"
        *                    }
        *                },
        *                "template_id": 1,
        *                "offer_id": 1,
        *                "requisites": {
        *                    "Card Number": "4111111111111111"
        *                },
        *                "description": "На карту сбербанка, только с карты на карту!!!",
        *                "created": 1468864432,
        *                "updated": 1468864432
        *            }],
        *            "course": 45000,
        *            "created": 1468864432,
        *            "updated": 1468864432
        *        }]
        *    }        
        */
        offers.Get("", APIOfferGetListV1)

        /**
        * @api {get} /v1/offers/search Поиск предложений о купле/продаже биткоинов
        * @apiName SearchOffers
        * @apiGroup Offers
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {Number} [limit=20] Количество запрашиваемых предложений
        * @apiParam {Number} [offset=0] Смещение запрашиваемых предложений        
        * @apiParam {Number[]} [filter[id]] Фильтр по идентификаторам (Условие: id in (filter[id]))
        * @apiParam {Number[]} [filter[currency_id]] Фильтр по идентификаторам фиатной валюты (Условие: currency_id in (filter[currency_id]))
        * @apiParam {String} [filter[type]] Фильтр по типу предложения (Условие: type = filter[type])
        * @apiParam {Number} [min[id]] Минимальное значение ID преложения (Условие: id >= min[id])
        * @apiParam {Number} [max[id]] Максимальное значение ID преложения (Условие: id <= max[id])
        * @apiParam {Number} [min[amount]] Минимальное значение суммы преложения (Условие: amount >= min[amount])
        * @apiParam {Number} [max[amount]] Максимальное значение суммы преложения (Условие: amount <= max[amount])
        * @apiParam {Number} [min[min_amount]] Минимальное значение минимальной суммы преложения (Условие: min_amount >= min[min_amount])
        * @apiParam {Number} [max[min_amount]] Максимальное значение минимальной суммы преложения (Условие: min_amount <= max[min_amount])
        * @apiParam {Number} [min[course]] Минимальное значение заявленного курса BTC (Условие: course >= min[course])
        * @apiParam {Number} [max[course]] Максимальное значение заявленного курса BTC (Условие: course <= max[course])
        * @apiParam {Number} [order[id]] Направление сортировки по ID
        * @apiParam {Number} [order[currency_id]] Направление сортировки по идентификатору фиатной валюты
        * @apiParam {Number} [order[created]] Направление сортировки по дате создания предложений
        * @apiParam {Number} [order[updated]] Направление сортировки по дате обновления предложений (лучше использовать вместо order[created])
        * @apiParam {Number} [order[amount]] Направление сортировки по сумме сделки
        * @apiParam {Number} [order[min_amount]] Направление сортировки по минимальной сумме сделки
        * @apiParam {Number} [order[course]] Направление сортировки по сумме заявленного курса BTC        
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object[]} result Объекты предложений (model: Offer)
        * @apiSuccess (200) {Number} result.id Идентификатор предложения        
        * @apiSuccess (200) {String="sell","buy"} result.type Тип предложения
        * @apiSuccess (200) {String} result.description Описание предложения (Сопроводительный текст или условия)
        * @apiSuccess (200) {Object} result.user Данные пользователя разместившего предложение (model:User)
        * @apiSuccess (200) {String} result.user.uuid Уникальный UUID пользователя        
        * @apiSuccess (200) {Number} result.amount Cумма сделки в BTC
        * @apiSuccess (200) {Number} result.min_amount Минимальная сумма сделки в BTC
        * @apiSuccess (200) {Number} result.currency_id Идентификатор фиатной валюты
        * @apiSuccess (200) {Object} result.currency Объект фиатной валюты (model: Currency)
        * @apiSuccess (200) {Number} result.currency.id Идентификатор фиатной валюты
        * @apiSuccess (200) {String} result.currency.code Трех символьный код фиатной валюты (RUS, EUR...)
        * @apiSuccess (200) {String} result.currency.symbol Unicode символ фиатной валюты ($...)
        * @apiSuccess (200) {Number} result.currency.buy_course Текущий курс BTC на покупку
        * @apiSuccess (200) {Number} result.currency.sell_course Текущий курс BTC на продажу
        * @apiSuccess (200) {Number} result.course Предпочтительный курс фиатной валюты (сумма за 1 BTC)
        * @apiSuccess (200) {Object[]} result.exchange_methods Доступные методы обмена фиатной валюты на BTC (model:ExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_methods.id Идентификатор метода обмена
        * @apiSuccess (200) {Number} result.exchange_methods.offer_id Идентификатор предложения
        * @apiSuccess (200) {Number} result.exchange_methods.template_id Идентификатор шаблона обмена
        * @apiSuccess (200) {Object} result.exchange_methods.template Объект шаблона обмена (model: TemplateExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_methods.template.id Идентификатор шаблона обмена
        * @apiSuccess (200) {String} result.exchange_methods.template.name Название шаблона обмена
        * @apiSuccess (200) {Bool} result.exchange_methods.template.online Есть ли возможность онлайн перевода
        * @apiSuccess (200) {String} result.exchange_methods.template.url Ссылка на сайт для осуществления перевода
        * @apiSuccess (200) {Object} result.exchange_methods.template.validator JSON с RegExp проверками параметров обмена)
        * @apiSuccess (200) {String} result.exchange_methods.description Описание / Сопроводительный текст / Условия
        * @apiSuccess (200) {Object} result.exchange_methods.requisites JSON параметров обмена в соответствии с выбранным шаблоном
        * @apiSuccess (200) {String} result.created Дата создания предложения
        * @apiSuccess (200) {String} result.updated Дата изменения предложения
        * @apiSuccess (200) {String} [result.validated] Дата допуска предложения к платформе
        * @apiSuccess (200) {String} [result.closed] Дата удаления / отмены предложения       
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        */
        offers.Get("/search", APIOfferSearchV1)
    }
}