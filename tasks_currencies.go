package main

import (    
    "time"
    "runtime"
    "net/http"    
    "encoding/json"        
    "github.com/kataras/iris"
)

// StartUpdateCurrencyCurses - запуск отслеживания курсов биткоина в русском регионе
func StartUpdateCurrencyCurses(db *Database) {
    if db != nil {        
        go func(db *Database) {
            orm := db.GetORM(true)
            for {
                if orm != nil {
                    resp, err := http.Get("https://blockchain.info/ru/ticker")
                    if err == nil {
                        var tickers iris.Map
                        decoder := json.NewDecoder(resp.Body)
                        err = decoder.Decode(&tickers)
                        if err == nil {
                            var currencies []Currency
                            orm.Find(&currencies)
                            for _, currency := range currencies {
                                if tickers[currency.Code] != nil {
                                    mp := tickers[currency.Code].(map[string]interface{})
                                    if len(mp) > 0 {
                                        if mp["buy"] != nil {
                                            currency.BuyCourse = mp["buy"].(float64)
                                        }
                                        if mp["sell"] != nil {
                                            currency.SellCourse = mp["sell"].(float64)
                                        }
                                        orm.Save(&currency)
                                    }                                
                                }
                            }
                        }
                    }
                } else {
                    orm = db.GetORM(true)
                }
                runtime.Gosched()
                time.Sleep(time.Minute * 30)
            }         
        }(db)       
    }
}