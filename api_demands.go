package main

import (       
    "github.com/jinzhu/gorm"
    "github.com/kataras/iris"
    "github.com/valyala/fasthttp"
    "github.com/legion-zver/utils/unixtime"
)

/* Модели запросов */

// RequestCreateDemandV1 - запрос создания запроса на предложение
type RequestCreateDemandV1 struct {
    OfferID               int      `form:"offer_id" json:"offer_id"`
    ExchangeMethodID      int      `form:"exchange_method_id" json:"exchange_method_id"`
    BitcoinAddress       *string   `form:"bitcoin_address" json:"bitcoin_address,omitempty"`
    Description           string   `form:"description" json:"description"`
    Amount                float64  `form:"amount" json:"amount"`
    FiatAmount            float64  `form:"amount" json:"fiat_amount"`     
} 

// Validation - валидация RequestCreateDemandV1 
func (r *RequestCreateDemandV1) Validation() bool {
    return r.OfferID > 0 && r.ExchangeMethodID > 0 && r.Amount > 0.0 && r.FiatAmount > 0.0
}

/* Для запросов */

// SetSortDemandsFromURL - установить сортировку из параметров URL в текущий GORM запрсов 
func SetSortDemandsFromURL(orm *gorm.DB, urlParams *fasthttp.Args) *gorm.DB {
    if orm != nil && urlParams != nil {
        if urlParams.Has("order[id]") {
            orm = orm.Order("id "+GetOrderDirect(urlParams.GetUfloatOrZero("order[id]") > 0))
        }
        if urlParams.Has("order[offer_id]") {
            orm = orm.Order("offer_id "+GetOrderDirect(urlParams.GetUfloatOrZero("order[offer_id]") > 0))
        }
        if urlParams.Has("order[amount]") {
            orm = orm.Order("amount "+GetOrderDirect(urlParams.GetUfloatOrZero("order[amount]") > 0))
        }
        if urlParams.Has("order[fiat_amount]") {
            orm = orm.Order("fiat_amount "+GetOrderDirect(urlParams.GetUfloatOrZero("order[fiat_amount]") > 0))
        }
        if urlParams.Has("order[exchange_method_id]") {
            orm = orm.Order("exchange_method_id "+GetOrderDirect(urlParams.GetUfloatOrZero("order[exchange_method_id]") > 0))
        }
        if urlParams.Has("order[created]") {
            orm = orm.Order("created "+GetOrderDirect(urlParams.GetUfloatOrZero("order[created]") > 0))
        }
    }
    return orm
}

/* Запросы */

// APIDemandsCreateV1 - создание предложения
func APIDemandsCreateV1(c *iris.Context) {
    var db      *Database
    var token   *Token
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)        
        if orm != nil {
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)            
            }
            if token != nil && token.UserID > 0 {
                var request RequestCreateDemandV1
                err := c.ReadJSON(&request)
                if err != nil {
                    c.JSON(iris.StatusBadRequest, iris.Map{"error":NewAPIError(400,[]APIErrorCause{APIErrorCause{Target:"request",Сause:err.Error()}})})
                    return
                }
                if request.Validation() {
                    // Начинаем получать и проверять данные запроса
                    var offer Offer
                    var method ExchangeMethod
                    orm.Where("id = ? AND user_id != ? AND closed IS NULL AND validated IS NOT NULL", 
                              request.OfferID, token.UserID).First(&offer)

                    if offer.ID != request.OfferID {
                        c.JSON(iris.StatusBadRequest, iris.Map{"error":NewAPIError(400,[]APIErrorCause{APIErrorCause{Target:"offer",Сause:"Not found"}})})
                        return
                    }
                    orm.Where("id = ? AND offer_id = ?", request.ExchangeMethodID, request.OfferID).First(&method)
                    if method.ID != request.ExchangeMethodID {
                        c.JSON(iris.StatusBadRequest, iris.Map{"error":NewAPIError(400,[]APIErrorCause{APIErrorCause{Target:"exchange_method",Сause:"Not found"}})})
                        return
                    }
                    // Проверка наличия кошелька у пользователя
                    if request.BitcoinAddress == nil {
                        token.User = new(User)
                        orm.Where("id = ?", token.UserID).First(token.User)
                        if token.User.WalletID <= 0 {
                            c.JSON(iris.StatusBadRequest, iris.Map{"error":NewAPIError(400,[]APIErrorCause{APIErrorCause{Target:"user.wallet",Сause:"Not found, please init wallet or set bitcount_address in request"}})})
                            return
                        }
                    }
                    // Проверка суммы предложения
                    if request.Amount <= offer.Amount && request.Amount >= offer.MinAmount {
                        demand := Demand{UserID: token.UserID, OfferID: request.OfferID, 
                                         ExchangeMethodID: request.ExchangeMethodID, Description: request.Description,
                                         Amount: request.Amount, FiatAmount: request.FiatAmount}
                        if request.BitcoinAddress != nil {
                            demand.BitcoinAddress = request.BitcoinAddress
                        }
                        demand.Created = unixtime.Now()                        
                        orm.Create(&demand)
                        if demand.ID > 0 {                            
                            // Устанавливаем зависимые элементы для вывода
                            demand.Offer          = &offer
                            demand.ExchangeMethod = &method
                            // Вывод
                            c.JSON(iris.StatusCreated, iris.Map{"error":nil, "result":demand})
                            return
                        }
                        // Ошибка создания
                        c.JSON(iris.StatusInternalServerError, iris.Map{"error":NewSimpleAPIError(41)})
                        return         
                    }
                }
                // Не прошли проверку валидации, запрет
                c.JSON(iris.StatusForbidden, iris.Map{"error":NewSimpleAPIError(29)})
                return
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

// APISelectDemands - функциия выборки запросов
func APISelectDemands(c *iris.Context, byOffers bool, byID bool) {
    var db      *Database
    var token   *Token
    if c.Get("database") != nil {
        db = c.Get("database").(*Database)
    }
    if db != nil {
        orm := db.GetORM(true)        
        if orm != nil {
            if c.Get("token") != nil {
                token = c.Get("token").(*Token)
            }
            if token != nil && token.UserID > 0 {
                urlParams := c.QueryArgs()
                var demands []Demand
                var limit, offset = 20, 0
                var ID int64
                if byOffers && byID {
                    var err error
                    ID, err = c.ParamInt64("id")
                    if err != nil {
                        c.JSON(iris.StatusBadRequest, iris.Map{"error":NewAPIError(29, []APIErrorCause{APIErrorCause{Target:"id", Сause:err.Error()}})})
                        return
                    }
                }
                // Лимиты и смещение
                if urlParams.Has("limit") {
                    limit = urlParams.GetUintOrZero("limit")
                    if limit <= 0 {
                        limit = 20
                    }
                }
                if urlParams.Has("offset") {
                    offset = urlParams.GetUintOrZero("offset")                    
                }
                // MIN & MAX
                if urlParams.Has("min[id]") {
                    orm = orm.Where("id >= ?", urlParams.GetUfloatOrZero("min[id]"))
                }
                if urlParams.Has("max[id]") {
                    orm = orm.Where("id <= ?", urlParams.GetUfloatOrZero("max[id]"))
                }                
                orm = SetSortDemandsFromURL(orm, urlParams)

                if byOffers {
                    if byID {
                        orm = orm.Joins("JOIN offers ON offers.id = demands.offer_id").
                                  Where("offers.user_id = ? AND offers.id = ?", token.UserID, ID)
                    } else {
                        orm = orm.Joins("JOIN offers ON offers.id = demands.offer_id").
                                  Where("offers.user_id = ?", token.UserID)
                    }
                } else {
                    orm = orm.Where("user_id = ?", token.UserID)
                }            
                
                orm.Limit(limit).Offset(offset).
                    Preload("Offer").
                    Preload("ExchangeMethod").
                    Find(&demands)

                // Вывод                                    
                c.JSON(iris.StatusOK, iris.Map{"error":nil, "result":demands})
                return
            }
            // Ошибка авторизации
            c.JSON(iris.StatusUnauthorized, iris.Map{"error":NewSimpleAPIError(401)})
            return
        }
    }
    c.JSON(iris.StatusServiceUnavailable, iris.Map{"error":NewSimpleAPIError(503)})
}

// APIDemandsGetListV1 - список своих запросов (которые были отправлены)
func APIDemandsGetListV1(c *iris.Context) {
    APISelectDemands(c,false,false)
}

// APIDemandsGetListByOfferIDV1 - список запросов своим предложениям
func APIDemandsGetListByOfferIDV1(c *iris.Context) {
    APISelectDemands(c,true,true)
}

// APIDemandsGetListByOffersV1 - список запросов своим предложениям
func APIDemandsGetListByOffersV1(c *iris.Context) {
    APISelectDemands(c,true,false)
}

/* Инициализация API */

// InitAPIDemands - инициализация API для спроса на предложения
func InitAPIDemands(api iris.MuxAPI, version int) {
    offers := api.Party("/demands")
    if version == 1 {
        /**
        * @api {post} /v1/demands Создание запроса на предложение о купле/продаже биткоинов
        * @apiName CreateDemand
        * @apiGroup Demands
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {Number} offer_id Идентификатор предложения (только предложения других пользователей)
        * @apiParam {Number} exchange_method_id Идентификатор метода обмена заинтересовавшего в предложении
        * @apiParam {String} description Описание запроса (Сопроводительный текст или условия)
        * @apiParam {Number} amount Cумма предлагаемой сделки в BTC
        * @apiParam {Number} fiat_amount Cумма предлагаемой сделки в фиатной валюте предложения                
        *
        * @apiSuccess (201) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (201) {Object} result Объект запроса (model: Demand)
        * @apiSuccess (201) {Number} result.id Идентификатор запроса        
        * @apiSuccess (201) {String} result.description Описание запроса (Сопроводительный текст или условия)
        * @apiSuccess (201) {Number} result.amount Cумма предлагаемой сделки в BTC
        * @apiSuccess (201) {Number} result.fiat_amount Cумма предлагаемой сделки в фиатной валюте предложения
        * @apiSuccess (201) {Number} result.offer_id Идентификатор предложения
        * @apiSuccess (201) {Object} result.offer Объект предложения (model: Offer)
        * @apiSuccess (201) {Number} result.offer.id Идентификатор предложения
        * @apiSuccess (201) {String="sell","buy"} result.offer.type Тип предложения
        * @apiSuccess (201) {String} result.offer.description Описание предложения (Сопроводительный текст или условия)
        * @apiSuccess (201) {Number} result.offer.amount Cумма сделки предложения в BTC
        * @apiSuccess (201) {Number} result.offer.min_amount Минимальная сумма сделки предложения в BTC
        * @apiSuccess (201) {Number} result.offer.currency_id Идентификатор фиатной валюты предложения
        * @apiSuccess (201) {String} result.offer.created Дата создания предложения
        * @apiSuccess (201) {String} result.offer.updated Дата изменения предложения
        * @apiSuccess (201) {String} [result.offer.validated] Дата допуска предложения к платформе
        * @apiSuccess (201) {String} [result.offer.closed] Дата удаления / отмены предложения        
        * @apiSuccess (201) {Object} result.exchange_method_id Идентификатор выбранного метода обмена фиатной валюты на BTC
        * @apiSuccess (201) {Object} result.exchange_method Выбранный метод обмена фиатной валюты на BTC (model:ExchangeMethod)
        * @apiSuccess (201) {Number} result.exchange_method.id Идентификатор метода обмена
        * @apiSuccess (201) {Number} result.exchange_method.offer_id Идентификатор предложения
        * @apiSuccess (201) {Number} result.exchange_method.template_id Идентификатор шаблона обмена
        * @apiSuccess (201) {Object} result.exchange_method.template Объект шаблона обмена (model: TemplateExchangeMethod)
        * @apiSuccess (201) {Number} result.exchange_method.template.id Идентификатор шаблона обмена
        * @apiSuccess (201) {String} result.exchange_method.template.name Название шаблона обмена
        * @apiSuccess (201) {Bool} result.exchange_method.template.online Есть ли возможность онлайн перевода
        * @apiSuccess (201) {String} result.exchange_method.template.url Ссылка на сайт для осуществления перевода
        * @apiSuccess (201) {Object} result.exchange_method.template.validator JSON с RegExp проверками параметров обмена)
        * @apiSuccess (201) {String} result.exchange_method.description Описание / Сопроводительный текст / Условия
        * @apiSuccess (201) {Object} result.exchange_method.requisites JSON параметров обмена в соответствии с выбранным шаблоном
        * @apiSuccess (201) {String} result.created Дата создания запроса
        * @apiSuccess (201) {String} [result.closed] Дата отмены запроса / либо удаления        
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 201 CREATE
        *    {
        *        "error": null,
        *        "result": {
        *            "id": 1,
        *            "offer": {
        *                "id": 1,
        *                "type": "sell",
        *                "description": "Продам биткоины, срочно! Только за рубли",
        *                "amount": 10,
        *                "min_amount": 0.5,
        *                "currency_id": 1,
        *                "course": 45000,
        *                "created": 1468864432,
        *                "updated": 1468864432,
        *                "validated": 1470231182
        *            },
        *            "offer_id": 1,
        *            "exchange_method": {
        *                "id": 1,
        *                "template_id": 1,
        *                "offer_id": 1,
        *                "requisites": {
        *                    "Card Number": "4111111111111111"
        *                },
        *                "description": "На карту сбербанка, только с карты на карту!!!",
        *                "created": 1468864432,
        *                "updated": 1468864432
        *            },
        *            "exchange_method_id": 1,
        *            "description": "Тестовый запрос",
        *            "amount": 1,
        *            "fiat_amount": 100,
        *            "created": 1470231252
        *        }
        *    }      
        */
        offers.Post("", APIDemandsCreateV1)

        /**
        * @api {get} /v1/demands Получить список своих запросов
        * @apiName GetListMyDemands
        * @apiGroup Demands
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {Number} [limit=20] Количество запрашиваемых запросов
        * @apiParam {Number} [offset=0] Смещение запрашиваемых запросов
        * @apiParam {Number} [min[id]] Минимальное значение ID запроса (Условие: id >= min[id])
        * @apiParam {Number} [max[id]] Максимальное значение ID запроса (Условие: id <= max[id])
        * @apiParam {Number[]} [filter[id]] Фильтр по идентификаторам (Условие: id in (filter[id]))        
        * @apiParam {Number} [order[id]] Направление сортировки по ID
        * @apiParam {Number} [order[offer_id]] Направление сортировки по идентификатору предложения
        * @apiParam {Number} [order[exchange_method_id]] Направление сортировки по идентификатору метода обмена запроса
        * @apiParam {Number} [order[created]] Направление сортировки по дате создания предложений        
        * @apiParam {Number} [order[amount]] Направление сортировки по предлагаемой сумме сделки в BTC
        * @apiParam {Number} [order[fiat_amount]] Направление сортировки по предлагаемой сумме сделки в фиатной валюте        
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object[]} result Объекты запросов (model: Demand)
        * @apiSuccess (200) {Number} result.id Идентификатор запроса        
        * @apiSuccess (200) {String} result.description Описание запроса (Сопроводительный текст или условия)
        * @apiSuccess (200) {Number} result.amount Cумма предлагаемой сделки в BTC
        * @apiSuccess (200) {Number} result.fiat_amount Cумма предлагаемой сделки в фиатной валюте предложения
        * @apiSuccess (200) {Number} result.offer_id Идентификатор предложения
        * @apiSuccess (200) {Object} result.offer Объект предложения (model: Offer)
        * @apiSuccess (200) {Number} result.offer.id Идентификатор предложения
        * @apiSuccess (200) {String="sell","buy"} result.offer.type Тип предложения
        * @apiSuccess (200) {String} result.offer.description Описание предложения (Сопроводительный текст или условия)
        * @apiSuccess (200) {Number} result.offer.amount Cумма сделки предложения в BTC
        * @apiSuccess (200) {Number} result.offer.min_amount Минимальная сумма сделки предложения в BTC
        * @apiSuccess (200) {Number} result.offer.currency_id Идентификатор фиатной валюты предложения
        * @apiSuccess (200) {String} result.offer.created Дата создания предложения
        * @apiSuccess (200) {String} result.offer.updated Дата изменения предложения
        * @apiSuccess (200) {String} [result.offer.validated] Дата допуска предложения к платформе
        * @apiSuccess (200) {String} [result.offer.closed] Дата удаления / отмены предложения        
        * @apiSuccess (200) {Object} result.exchange_method_id Идентификатор выбранного метода обмена фиатной валюты на BTC
        * @apiSuccess (200) {Object} result.exchange_method Выбранный метод обмена фиатной валюты на BTC (model:ExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_method.id Идентификатор метода обмена
        * @apiSuccess (200) {Number} result.exchange_method.offer_id Идентификатор предложения
        * @apiSuccess (200) {Number} result.exchange_method.template_id Идентификатор шаблона обмена
        * @apiSuccess (200) {Object} result.exchange_method.template Объект шаблона обмена (model: TemplateExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_method.template.id Идентификатор шаблона обмена
        * @apiSuccess (200) {String} result.exchange_method.template.name Название шаблона обмена
        * @apiSuccess (200) {Bool} result.exchange_method.template.online Есть ли возможность онлайн перевода
        * @apiSuccess (200) {String} result.exchange_method.template.url Ссылка на сайт для осуществления перевода
        * @apiSuccess (200) {Object} result.exchange_method.template.validator JSON с RegExp проверками параметров обмена)
        * @apiSuccess (200) {String} result.exchange_method.description Описание / Сопроводительный текст / Условия
        * @apiSuccess (200) {Object} result.exchange_method.requisites JSON параметров обмена в соответствии с выбранным шаблоном
        * @apiSuccess (200) {String} result.created Дата создания запроса
        * @apiSuccess (200) {String} [result.closed] Дата отмены запроса / либо удаления        
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "result": [{
        *            "id": 1,
        *            "offer": {
        *                "id": 1,
        *                "type": "sell",
        *                "description": "Продам биткоины, срочно! Только за рубли",
        *                "amount": 10,
        *                "min_amount": 0.5,
        *                "currency_id": 1,
        *                "course": 45000,
        *                "created": 1468864432,
        *                "updated": 1468864432,
        *                "validated": 1470231182
        *            },
        *            "offer_id": 1,
        *            "exchange_method": {
        *                "id": 1,
        *                "template_id": 1,
        *                "offer_id": 1,
        *                "requisites": {
        *                    "Card Number": "4111111111111111"
        *                },
        *                "description": "На карту сбербанка, только с карты на карту!!!",
        *                "created": 1468864432,
        *                "updated": 1468864432
        *            },
        *            "exchange_method_id": 1,
        *            "description": "Тестовый запрос",
        *            "amount": 1,
        *            "fiat_amount": 100,
        *            "created": 1470231252
        *        }]
        *    }      
        */
        offers.Get("", APIDemandsGetListV1)

        /**
        * @api {get} /v1/demands/by-offers Получить список запросов к своим предложениям
        * @apiName GetListDemandsByMyOffers
        * @apiGroup Demands
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {Number} [limit=20] Количество запрашиваемых запросов
        * @apiParam {Number} [offset=0] Смещение запрашиваемых запросов
        * @apiParam {Number} [min[id]] Минимальное значение ID запроса (Условие: id >= min[id])
        * @apiParam {Number} [max[id]] Максимальное значение ID запроса (Условие: id <= max[id])
        * @apiParam {Number[]} [filter[id]] Фильтр по идентификаторам (Условие: id in (filter[id]))        
        * @apiParam {Number} [order[id]] Направление сортировки по ID
        * @apiParam {Number} [order[offer_id]] Направление сортировки по идентификатору предложения
        * @apiParam {Number} [order[exchange_method_id]] Направление сортировки по идентификатору метода обмена запроса
        * @apiParam {Number} [order[created]] Направление сортировки по дате создания предложений        
        * @apiParam {Number} [order[amount]] Направление сортировки по предлагаемой сумме сделки в BTC
        * @apiParam {Number} [order[fiat_amount]] Направление сортировки по предлагаемой сумме сделки в фиатной валюте        
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object[]} result Объекты запросов (model: Demand)
        * @apiSuccess (200) {Number} result.id Идентификатор запроса        
        * @apiSuccess (200) {String} result.description Описание запроса (Сопроводительный текст или условия)
        * @apiSuccess (200) {Number} result.amount Cумма предлагаемой сделки в BTC
        * @apiSuccess (200) {Number} result.fiat_amount Cумма предлагаемой сделки в фиатной валюте предложения
        * @apiSuccess (200) {Number} result.offer_id Идентификатор предложения
        * @apiSuccess (200) {Object} result.offer Объект предложения (model: Offer)
        * @apiSuccess (200) {Number} result.offer.id Идентификатор предложения
        * @apiSuccess (200) {String="sell","buy"} result.offer.type Тип предложения
        * @apiSuccess (200) {String} result.offer.description Описание предложения (Сопроводительный текст или условия)
        * @apiSuccess (200) {Number} result.offer.amount Cумма сделки предложения в BTC
        * @apiSuccess (200) {Number} result.offer.min_amount Минимальная сумма сделки предложения в BTC
        * @apiSuccess (200) {Number} result.offer.currency_id Идентификатор фиатной валюты предложения
        * @apiSuccess (200) {String} result.offer.created Дата создания предложения
        * @apiSuccess (200) {String} result.offer.updated Дата изменения предложения
        * @apiSuccess (200) {String} [result.offer.validated] Дата допуска предложения к платформе
        * @apiSuccess (200) {String} [result.offer.closed] Дата удаления / отмены предложения        
        * @apiSuccess (200) {Object} result.exchange_method_id Идентификатор выбранного метода обмена фиатной валюты на BTC
        * @apiSuccess (200) {Object} result.exchange_method Выбранный метод обмена фиатной валюты на BTC (model:ExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_method.id Идентификатор метода обмена
        * @apiSuccess (200) {Number} result.exchange_method.offer_id Идентификатор предложения
        * @apiSuccess (200) {Number} result.exchange_method.template_id Идентификатор шаблона обмена
        * @apiSuccess (200) {Object} result.exchange_method.template Объект шаблона обмена (model: TemplateExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_method.template.id Идентификатор шаблона обмена
        * @apiSuccess (200) {String} result.exchange_method.template.name Название шаблона обмена
        * @apiSuccess (200) {Bool} result.exchange_method.template.online Есть ли возможность онлайн перевода
        * @apiSuccess (200) {String} result.exchange_method.template.url Ссылка на сайт для осуществления перевода
        * @apiSuccess (200) {Object} result.exchange_method.template.validator JSON с RegExp проверками параметров обмена)
        * @apiSuccess (200) {String} result.exchange_method.description Описание / Сопроводительный текст / Условия
        * @apiSuccess (200) {Object} result.exchange_method.requisites JSON параметров обмена в соответствии с выбранным шаблоном
        * @apiSuccess (200) {String} result.created Дата создания запроса
        * @apiSuccess (200) {String} [result.closed] Дата отмены запроса / либо удаления        
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "result": [{
        *            "id": 1,
        *            "offer": {
        *                "id": 1,
        *                "type": "sell",
        *                "description": "Продам биткоины, срочно! Только за рубли",
        *                "amount": 10,
        *                "min_amount": 0.5,
        *                "currency_id": 1,
        *                "course": 45000,
        *                "created": 1468864432,
        *                "updated": 1468864432,
        *                "validated": 1470231182
        *            },
        *            "offer_id": 1,
        *            "exchange_method": {
        *                "id": 1,
        *                "template_id": 1,
        *                "offer_id": 1,
        *                "requisites": {
        *                    "Card Number": "4111111111111111"
        *                },
        *                "description": "На карту сбербанка, только с карты на карту!!!",
        *                "created": 1468864432,
        *                "updated": 1468864432
        *            },
        *            "exchange_method_id": 1,
        *            "description": "Тестовый запрос",
        *            "amount": 1,
        *            "fiat_amount": 100,
        *            "created": 1470231252
        *        }]
        *    }      
        */
        offers.Get("/by-offers", APIDemandsGetListByOffersV1)

        /**
        * @api {get} /v1/demands/by-offers/:id Получить список запросов к своему предложению по его идентификатору
        * @apiName GetListDemandsByMyOffer
        * @apiGroup Demands
        * @apiVersion 0.0.1
        * @apiPermission user
        *
        * @apiHeader {String} [Token] Токен доступа (можно передать в теле запроса)
        * @apiParam {String} [token] Токен доступа (можно передать в заголовке запросв)
        * @apiParam {Number} id Идентификатор своего предложения
        * @apiParam {Number} [limit=20] Количество запрашиваемых запросов
        * @apiParam {Number} [offset=0] Смещение запрашиваемых запросов
        * @apiParam {Number} [min[id]] Минимальное значение ID запроса (Условие: id >= min[id])
        * @apiParam {Number} [max[id]] Максимальное значение ID запроса (Условие: id <= max[id])
        * @apiParam {Number[]} [filter[id]] Фильтр по идентификаторам (Условие: id in (filter[id]))        
        * @apiParam {Number} [order[id]] Направление сортировки по ID
        * @apiParam {Number} [order[offer_id]] Направление сортировки по идентификатору предложения
        * @apiParam {Number} [order[exchange_method_id]] Направление сортировки по идентификатору метода обмена запроса
        * @apiParam {Number} [order[created]] Направление сортировки по дате создания предложений        
        * @apiParam {Number} [order[amount]] Направление сортировки по предлагаемой сумме сделки в BTC
        * @apiParam {Number} [order[fiat_amount]] Направление сортировки по предлагаемой сумме сделки в фиатной валюте        
        *
        * @apiSuccess (200) {Object} error=null Ошибок нет (model:Error)
        * @apiSuccess (200) {Object[]} result Объекты запросов (model: Demand)
        * @apiSuccess (200) {Number} result.id Идентификатор запроса        
        * @apiSuccess (200) {String} result.description Описание запроса (Сопроводительный текст или условия)
        * @apiSuccess (200) {Number} result.amount Cумма предлагаемой сделки в BTC
        * @apiSuccess (200) {Number} result.fiat_amount Cумма предлагаемой сделки в фиатной валюте предложения
        * @apiSuccess (200) {Number} result.offer_id Идентификатор предложения
        * @apiSuccess (200) {Object} result.offer Объект предложения (model: Offer)
        * @apiSuccess (200) {Number} result.offer.id Идентификатор предложения
        * @apiSuccess (200) {String="sell","buy"} result.offer.type Тип предложения
        * @apiSuccess (200) {String} result.offer.description Описание предложения (Сопроводительный текст или условия)
        * @apiSuccess (200) {Number} result.offer.amount Cумма сделки предложения в BTC
        * @apiSuccess (200) {Number} result.offer.min_amount Минимальная сумма сделки предложения в BTC
        * @apiSuccess (200) {Number} result.offer.currency_id Идентификатор фиатной валюты предложения
        * @apiSuccess (200) {String} result.offer.created Дата создания предложения
        * @apiSuccess (200) {String} result.offer.updated Дата изменения предложения
        * @apiSuccess (200) {String} [result.offer.validated] Дата допуска предложения к платформе
        * @apiSuccess (200) {String} [result.offer.closed] Дата удаления / отмены предложения        
        * @apiSuccess (200) {Object} result.exchange_method_id Идентификатор выбранного метода обмена фиатной валюты на BTC
        * @apiSuccess (200) {Object} result.exchange_method Выбранный метод обмена фиатной валюты на BTC (model:ExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_method.id Идентификатор метода обмена
        * @apiSuccess (200) {Number} result.exchange_method.offer_id Идентификатор предложения
        * @apiSuccess (200) {Number} result.exchange_method.template_id Идентификатор шаблона обмена
        * @apiSuccess (200) {Object} result.exchange_method.template Объект шаблона обмена (model: TemplateExchangeMethod)
        * @apiSuccess (200) {Number} result.exchange_method.template.id Идентификатор шаблона обмена
        * @apiSuccess (200) {String} result.exchange_method.template.name Название шаблона обмена
        * @apiSuccess (200) {Bool} result.exchange_method.template.online Есть ли возможность онлайн перевода
        * @apiSuccess (200) {String} result.exchange_method.template.url Ссылка на сайт для осуществления перевода
        * @apiSuccess (200) {Object} result.exchange_method.template.validator JSON с RegExp проверками параметров обмена)
        * @apiSuccess (200) {String} result.exchange_method.description Описание / Сопроводительный текст / Условия
        * @apiSuccess (200) {Object} result.exchange_method.requisites JSON параметров обмена в соответствии с выбранным шаблоном
        * @apiSuccess (200) {String} result.created Дата создания запроса
        * @apiSuccess (200) {String} [result.closed] Дата отмены запроса / либо удаления        
        *
        * @apiError {Object} error Обьект ошибки (model:Error)
        * @apiError {Number} error.code Код ошибки
        * @apiError {Object[]} error.causes Список причин ошибки
        * @apiError {String} error.causes.target Имя объекта причины
        * @apiError {String} error.causes.cause Описание проблеммы
        * @apiError {String} error.description Описание кода ошибки
        *
        * @apiErrorExample {json} Error-Response:
        *     HTTP/1.1 403 FORBIDDEN
        *     {
        *          "error": {
        *                "code": 403,
        *                "causes": [],
        *                "description": "Request params not validate"
        *          }
        *     }
        *
        * @apiSuccessExample {json} Success-Response:
        *    HTTP/1.1 200 OK
        *    {
        *        "error": null,
        *        "result": [{
        *            "id": 1,
        *            "offer": {
        *                "id": 1,
        *                "type": "sell",
        *                "description": "Продам биткоины, срочно! Только за рубли",
        *                "amount": 10,
        *                "min_amount": 0.5,
        *                "currency_id": 1,
        *                "course": 45000,
        *                "created": 1468864432,
        *                "updated": 1468864432,
        *                "validated": 1470231182
        *            },
        *            "offer_id": 1,
        *            "exchange_method": {
        *                "id": 1,
        *                "template_id": 1,
        *                "offer_id": 1,
        *                "requisites": {
        *                    "Card Number": "4111111111111111"
        *                },
        *                "description": "На карту сбербанка, только с карты на карту!!!",
        *                "created": 1468864432,
        *                "updated": 1468864432
        *            },
        *            "exchange_method_id": 1,
        *            "description": "Тестовый запрос",
        *            "amount": 1,
        *            "fiat_amount": 100,
        *            "created": 1470231252
        *        }]
        *    }      
        */
        offers.Get("/by-offers/:id", APIDemandsGetListByOfferIDV1)     
    }
}