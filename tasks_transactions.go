package main

import (    
    "time"
    "runtime"
    "github.com/btcsuite/btcd/wire"
    "github.com/legion-zver/utils/unixtime"
)

// StartValidationTransactions - запуск проверки транзакций
func StartValidationTransactions(db *Database, rpc *BitcoinJSONRPC) {
    if db != nil && rpc != nil {        
        go func(db *Database, rpc *BitcoinJSONRPC) {
            orm     := db.GetORM(true)
            client  := rpc.GetNewClient()
            for {                        
                now := unixtime.Now()
                if orm != nil && client != nil {
                    var transactions []WalletTransaction
                    orm.Where("сonfirmed IS NULL AND canceled IS NULL").Find(&transactions)
                    for _, transaction := range transactions {
                        hash, err := wire.NewShaHashFromStr(transaction.BitcoinTransaction)
                        if err != nil || hash == nil {
                            continue
                        }
                        tx, err := client.GetTransaction(hash)
                        if err != nil || tx == nil {
                            continue
                        }
                        // Если есть > 5 подтверждений от нод, то транзакция считается завершенной 
                        if tx.Confirmations > 5 {
                            transaction.Сonfirmed = &now
                            orm.Save(&transaction)
                        }                        
                    }
                } else { // Если соединения нет, то попробовать его пересоздать
                    if orm == nil {
                        orm = db.GetORM(true)
                    }
                    if client == nil {
                        client = rpc.GetNewClient()
                    }                    
                }
                // Проверка каждые 2 минуты
                runtime.Gosched()
                time.Sleep(time.Minute * 2)
            }
        }(db, rpc)       
    }
}