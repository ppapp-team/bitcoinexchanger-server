package main

import (
    "fmt"
    "github.com/jinzhu/gorm"     
    _ "github.com/jinzhu/gorm/dialects/mysql"   
)

// BaseDatabase - Интерфейс БД
type BaseDatabase interface {
    Init(config string) bool
    IsValid() bool    
    GetORM(createNew bool) *gorm.DB
    AutoMigrationModels()
    Destroy()
}

// Database - Класс БД
type Database struct {
    orm *gorm.DB
    config string
}

// IsValid - Проверка на валидность
func (s *Database) IsValid() bool {    
    return s.orm != nil
}

// GetORM - новое соединение
func (s *Database) GetORM(createNew bool) *gorm.DB {
    if !s.IsValid() {
        s.Init(s.config)
    }
    if s.orm != nil {
        if createNew {
            return s.orm.New()
        }
        return s.orm
    }
    return nil
}

// Init - Инициализация ORM базы данных
func (s *Database) Init(config string) bool {    
    s.config = config    
    var db, err = gorm.Open("mysql",s.config)
    if err != nil {
        s.orm = nil
        fmt.Println("Database Error: ", err.Error())
        return false
    }
    s.orm = db
    return s.orm != nil
}

// Destroy - Закрытие соединения с БД
func (s *Database) Destroy() {
    if s.orm != nil {
        s.orm.Close()
    }    
}