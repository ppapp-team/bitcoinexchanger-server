package main

import (    
    "errors"    
    "regexp"
    "reflect"
    "strings"    
    "encoding/json"    
    "github.com/legion-zver/utils/unixtime"
)

// GarantWallet - гарант кошелек (одноразовый)
type GarantWallet struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"-"`    
    BitcoinAddress        string                 `gorm:"type:char(34);not null;index" json:"bitcoin_address"`
    Created               unixtime.Time          `gorm:"type:int(11);not null" json:"created"`    
    Closed               *unixtime.Time          `gorm:"type:int(11)" json:"closed,omitempty"`
}

// Wallet - кошелек (50% API v1)
type Wallet struct {
    GarantWallet
    UUID                  string                 `gorm:"type:char(36);not null;index" json:"uuid"`
    User                 *User                   `json:"user,omitempty"`
    UserID                int                    `gorm:"not null;default:0;index" json:"-"`
    AccessKey            *string                 `gorm:"type:char(40)" json:"-"`
    OperationKey         *string                 `gorm:"type:char(40)" json:"-"`
    Transactions        []WalletTransaction      `gorn:"one2many" json:"transactions,omitempty"`    
    CheckSum              string                 `gorm:"type:char(40);not null" json:"-"`    
    Updated               unixtime.Time          `gorm:"type:int(11);not null" json:"updated"`    
}

// WalletTransaction - транзакции кошелька
type WalletTransaction struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"-"`
    WalletFrom           *Wallet                 `json:"wallet_from,omitempty"`
    WalletFromID          int                    `gorm:"not null;default:0;index" json:"-"`
    WalletFromUUID       *string                 `sql:"-" json:"wallet_from_uuid,omitempty"` // Расчетные значения
    WalletTo             *Wallet                 `json:"wallet_to,omitempty"`
    WalletToID            int                    `gorm:"not null;default:0;index" json:"-"`
    WalletToUUID         *string                 `sql:"-" json:"wallet_to_uuid,omitempty"`   // Расчетные значения
    BitcoinAddress       *string                 `gorm:"type:char(34);index" json:"bitcoin_address,omitempty"`
    BitcoinTransaction    string                 `gorm:"type:char(65);index" json:"bitcoin_transaction"`
    Amount                float64                `gorm:"type:decimal(12,8);default:0" json:"amount"`
    Created               unixtime.Time          `gorm:"type:int(11);not null" json:"created"`
    Сonfirmed            *unixtime.Time          `gorm:"type:int(11)" json:"сonfirmed,omitempty"`
    Canceled             *unixtime.Time          `gorm:"type:int(11)" json:"canceled,omitempty"`
}

// User - пользователь
type User struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"-"`
    UUID                  string                 `gorm:"type:char(36);not null;index" json:"uuid"`
    UniquePhrase         *string                 `gorm:"type:char(40)" json:"-"`
    Wallet               *Wallet                 `json:"wallet,omitempty"`
    WalletID              int                    `gorm:"not null;default:0;index" json:"-"`                                     
}

// Token - токен доступа для устройства (75% API v1)
type Token struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"-"`
    DeviceType            string                 `gorm:"type:ENUM('ios','android','wp','browser','other');not null;default:'other'" json:"device_type"`
    DeviceID              string                 `gorm:"type:varchar(64);not null" json:"device_id"`
    PushToken            *string                 `gorm:"type:varchar(128)" json:"push_token"`
    Token                 string                 `gorm:"type:char(40);not null" json:"token"`
    RefreshToken          string                 `gorm:"type:char(40);not null" json:"refresh_token"`    
    User                 *User                   `json:"user,omitempty"`
    UserID                int                    `gorm:"not null;default:0;index" json:"-"`
    Created               unixtime.Time          `gorm:"type:int(11);not null" json:"created"`
    Updated               unixtime.Time          `gorm:"type:int(11);not null" json:"updated"`
    Closed               *unixtime.Time          `gorm:"type:int(11)" json:"closed,omitempty"`
}

// CurrencyName - название фиатной валюты с учетом локали языка (100% API v1)
type CurrencyName struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"-"`
    CurrencyID            int                    `gorm:"not null;default:0;index" json:"-"`
    LanguageLocale        string                 `gorm:"type:char(3)" json:"language_locale"`
    Title                 string                 `gorm:"type:varchar(32)" json:"title"`
}

// Currency - фиатная валюта (75% API v1)
type Currency struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"id"`
    Names               []CurrencyName           `gorm:"one2many" json:"names,omitempty"`
    Code                  string                 `gorm:"type:char(3);not null" json:"code"`
    Symbol                string                 `gorm:"type:varchar(3);not null" json:"symbol"`
    BuyCourse             float64                `gorm:"type:decimal(12,4);default:0" json:"buy_course"`
    SellCourse            float64                `gorm:"type:decimal(12,4);default:0" json:"sell_course"`    
}

// TemplateExchangeMethod - Шаблон метода перевода фиатной валюты
type TemplateExchangeMethod struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"id"`
    Name                  string                 `gorm:"type:varchar(64);not null" json:"name"`
    Online                bool                   `gorm:"not null;default:0" json:"online"`
    URL                   string                 `gorm:"type:varchar(256);not null" json:"url"`
    Validator             string                 `gorm:"type:text;not null" json:"-"`
    ValidatorJSON         map[string]interface{} `sql:"-" json:"validator"`
}

// Offer - предложение покупки / продажи
type Offer struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"id"`
    Type                  string                 `gorm:"type:ENUM('sell','buy');not null;default:'sell';index" json:"type"`
    User                 *User                   `json:"user,omitempty"`
    UserID                int                    `gorm:"not null;default:0;index" json:"-"`
    Description           string                 `gorm:"type:varchar(256);not null" json:"description"`
    BitcoinAddress       *string                 `gorm:"type:char(34);default:NULL;index" json:"bitcoin_address,omitempty"`
    Amount                float64                `gorm:"type:decimal(12,8);default:0" json:"amount"`
    MinAmount             float64                `gorm:"type:decimal(12,8);default:0" json:"min_amount"`
    Currency             *Currency               `json:"currency,omitempty"`
    CurrencyID            int                    `gorm:"not null;default:0;index" json:"currency_id"`
    ExchangeMethods     []ExchangeMethod         `gorn:"one2many" json:"exchange_methods,omitempty"`   
    Course                float64                `gorm:"type:decimal(12,4);default:0" json:"course"`
    Created               unixtime.Time          `gorm:"type:int(11);not null" json:"created"`
    Updated               unixtime.Time          `gorm:"type:int(11);not null" json:"updated"`
    Validated            *unixtime.Time          `gorm:"type:int(11);default:NULL" json:"validated,omitempty"`
    Closed               *unixtime.Time          `gorm:"type:int(11);default:NULL" json:"closed,omitempty"`
}

// ExchangeMethod - метод перевода
type ExchangeMethod struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"id"`
    Template             *TemplateExchangeMethod `json:"template,omitempty"`
    TemplateID            int                    `gorm:"not null;default:0;index" json:"template_id"`    
    Offer                *Offer                  `json:"offer,omitempty"`
    OfferID               int                    `gorm:"not null;index" json:"offer_id"`
    Requisites            string                 `gorm:"type:text;not null" json:"-"`
    RequisitesJSON        map[string]interface{} `sql:"-" json:"requisites"`
    Description           string                 `gorm:"type:varchar(256);not null" json:"description"`
    Created               unixtime.Time          `gorm:"type:int(11);not null" json:"created"`
    Updated               unixtime.Time          `gorm:"type:int(11);not null" json:"updated"` 
}

// Demand - спрос / запрос на предложение
type Demand struct {
    ID                    int                    `gorm:"AUTO_INCREMENT;primary_key" json:"id"`
    User                 *User                   `json:"user,omitempty"`
    UserID                int                    `gorm:"not null;default:0;index" json:"-"`
    Offer                *Offer                  `json:"offer,omitempty"`
    OfferID               int                    `gorm:"not null;index" json:"offer_id"`
    ExchangeMethod       *ExchangeMethod         `json:"exchange_method,omitempty"`
    ExchangeMethodID      int                    `gorm:"not null;index" json:"exchange_method_id"`
    BitcoinAddress       *string                 `gorm:"type:char(34);default:NULL;index" json:"bitcoin_address,omitempty"`
    Description           string                 `gorm:"type:varchar(256);not null" json:"description"`
    Amount                float64                `gorm:"type:decimal(12,8);default:0" json:"amount"`
    FiatAmount            float64                `gorm:"type:decimal(12,4);default:0" json:"fiat_amount"`
    Created               unixtime.Time          `gorm:"type:int(11);not null" json:"created"`    
    Closed               *unixtime.Time          `gorm:"type:int(11);default:NULL" json:"closed,omitempty"`
}

// Deal - Сделка
type Deal struct {
    ID                      int                    `gorm:"AUTO_INCREMENT;primary_key" json:"-"`
    UUID                    string                 `gorm:"type:char(36);not null;index" json:"uuid"`
    Demand                 *Demand                 `json:"demand,omitempty"`
    DemandID                int                    `gorm:"not null;default:0;index" json:"demand_id"`
    CommissionAmount        float64                `gorm:"type:decimal(12,8);default:0" json:"commission_amount"`
    GarantWallet           *GarantWallet           `json:"garant_wallet,omitempty"`
    GarantWalletID          int                    `gorm:"not null;default:0;index" json:"-"`
    CheckSum                string                 `gorm:"type:char(40);not null" json:"-"`
    Created                 unixtime.Time          `gorm:"type:int(11);not null" json:"created"`    
    Expectation             unixtime.Time          `gorm:"type:int(11);not null" json:"expectation"`
    Opened                 *unixtime.Time          `gorm:"type:int(11)" json:"opened"`
    FiatVerifiedPartyOffer *unixtime.Time          `gorm:"type:int(11);default:NULL" json:"fiat_verified_party_offer,omitempty"`
    FiatVerifiedPartyDeman *unixtime.Time          `gorm:"type:int(11);default:NULL" json:"fiat_verified_party_demand,omitempty"`
    Closed                 *unixtime.Time          `gorm:"type:int(11);default:NULL" json:"closed,omitempty"`
}

// AdministrationUser - Пользователь для документации
type AdministrationUser struct {
    ID                      int                    `gorm:"AUTO_INCREMENT;primary_key" json:"-"`
    RoleID                  int                    `gorm:"not null;default:0;index" json:"-"`
    Login                   string                 `gorm:"type:char(255);not null;index" json:"login"`
    Password                string                 `gorm:"type:char(255);not null;index" json:"-"`
    Email                   string                 `gorm:"type:char(255);not null" json:"email"`
    RememberToken           string                 `gorm:"type:char(100);not null;index" json:"token"`
    CreatedAt               unixtime.Time          `gorm:"type:TIMESTAMP;not null" json:"created_at"`
    UpdatedAt               unixtime.Time          `gorm:"type:TIMESTAMP;not null" json:"updated_at"`
}

// StructValidation (ExchangeMethod) - проверка модели на правильность
func (method *ExchangeMethod) StructValidation() error {
    if len(method.RequisitesJSON) <= 0 {
        return errors.New("Exchange method contains no requisites JSON")
    }
    if method.TemplateID <= 0 {
        if method.Template == nil || method.Template.ID <= 0 {
            return errors.New("Exchange method contains no template link data")
        }
    }
    return nil
}

// StructValidation (Offer) - проверка модели на правильность
func (offer *Offer) StructValidation() error {
    if offer != nil {
        if strings.Compare(offer.Type,"sell")!=0 && strings.Compare(offer.Type,"buy")!=0 {
            return errors.New("Offer type \""+offer.Type+"\" invalidate, allowed only \"sell\" or \"buy\"")
        }        
        if offer.UserID <= 0 {
            return errors.New("Offer contains no user data")
        }
        if offer.Amount <= 0 || offer.MinAmount <= 0 {
            return errors.New("The Amount of offers and the minimum Amount may not be less than or equal to 0.0")
        }
        if offer.Course <= 0 {
            return errors.New("The Course of offers may not be less than or equal to 0")
        }        
        if offer.CurrencyID <= 0 {
            return errors.New("Offer contains no currency fiat money data")
        }
        if offer.ExchangeMethods == nil || len(offer.ExchangeMethods) <= 0 {
            return errors.New("Offer contains no exchange methods data")
        }
        for _, method := range offer.ExchangeMethods {
            if err := method.StructValidation(); err != nil {
                return err
            }
        }
    }
    return nil
}

// MethodValidation (TemplateExchangeMethod) - проверка метода по текущему шаблону 
func (template *TemplateExchangeMethod) MethodValidation(method *ExchangeMethod) error {
    if len(template.ValidatorJSON) <= 0 || template.ValidatorJSON == nil{
        return errors.New("ValidatorJSON empty")
    }
    if len(method.RequisitesJSON) <= 0 || method.RequisitesJSON == nil{
        return errors.New("RequisitesJSON empty")
    }
    for key, value := range template.ValidatorJSON {
        if method.RequisitesJSON[key] == nil {
            return errors.New("Key \""+key+"\" not find in RequisitesJSON")
        }
        if reflect.TypeOf(value).Kind() == reflect.String && reflect.TypeOf(method.RequisitesJSON[key]).Kind() == reflect.String {
            validator, err := regexp.Compile(value.(string))
            if err != nil {
                return err
            }
            if validator == nil {
                return errors.New("Template value for key \""+key+"\" not must compile")
            }
            if !validator.MatchString(method.RequisitesJSON[key].(string)) {
                return errors.New("Template value for key \""+key+"\" not match value in RequisitesJSON")
            }
        } else {
            return errors.New("Values for key \""+key+"\" not strings")
        }
    }
    return nil
} 

/* CALLBACK FUNCTIONS */

// AfterFind (TemplateExchangeMethod) - при выборки
func (template *TemplateExchangeMethod) AfterFind() (err error) {
    template.ValidatorJSON = make(map[string]interface{},0)
    json.Unmarshal([]byte(template.Validator),&template.ValidatorJSON)    
    return
}

// AfterFind (TemplateExchangeMethod) - при выборки
func (method *ExchangeMethod) AfterFind() (err error) {
    method.RequisitesJSON = make(map[string]interface{},0)
    json.Unmarshal([]byte(method.Requisites),&method.RequisitesJSON)    
    return
}

// BeforeCreate (TemplateExchangeMethod) - до изменения
func (template *TemplateExchangeMethod) BeforeCreate() (err error) {
    if len(template.ValidatorJSON) <= 0 {
        err = errors.New("ValidataionJSON is empty")
    }
    bytes, err := json.Marshal(template.ValidatorJSON)
    if err == nil {
        template.Validator = string(bytes)
    }
    return
}

// BeforeUpdate (TemplateExchangeMethod) - до изменения
func (template *TemplateExchangeMethod) BeforeUpdate() (err error) {
    if len(template.ValidatorJSON) <= 0 {
        err = errors.New("ValidataionJSON is empty")
    }
    bytes, err := json.Marshal(template.ValidatorJSON)
    if err == nil {
        template.Validator = string(bytes)
    }
    return
}

// BeforeCreate (TemplateExchangeMethod) - до изменения
func (method *ExchangeMethod) BeforeCreate() (err error) {
    if len(method.RequisitesJSON) <= 0 {
        err = errors.New("RequisitesJSON is empty")
    }
    bytes, err := json.Marshal(method.RequisitesJSON)
    if err == nil {
        method.Requisites = string(bytes)
    }
    return
}

// BeforeUpdate (TemplateExchangeMethod) - до изменения
func (method *ExchangeMethod) BeforeUpdate() (err error) {
    if len(method.RequisitesJSON) <= 0 {
        err = errors.New("RequisitesJSON is empty")
    }
    bytes, err := json.Marshal(method.RequisitesJSON)
    if err == nil {
        method.Requisites = string(bytes)
    }
    return
}

// AutoMigrationModels - Автомиграция всех моделей данных
func (s *Database) AutoMigrationModels() {
    if(s.IsValid()) {
        // Автомиграция
        s.orm/*.LogMode(true)*/.AutoMigrate(GarantWallet{}, WalletTransaction{}, Wallet{}, User{}, Token{}, 
                          CurrencyName{}, Currency{}, TemplateExchangeMethod{},
                          Offer{}, ExchangeMethod{}, Demand{}, Deal{},
                          AdministrationUser{})

        // Удаление триггеров
        s.orm.Exec("DROP TRIGGER IF EXISTS UUID_WALLET;")
        s.orm.Exec("DROP TRIGGER IF EXISTS UUID_USER;")
        s.orm.Exec("DROP TRIGGER IF EXISTS UUID_DEAL;")
        
        // Создание тригеров в БД
        s.orm.Exec("CREATE TRIGGER UUID_WALLET BEFORE INSERT ON wallets FOR EACH ROW SET NEW.uuid = UUID();")        
        s.orm.Exec("CREATE TRIGGER UUID_USER BEFORE INSERT ON users FOR EACH ROW SET NEW.uuid = UUID();")
        s.orm.Exec("CREATE TRIGGER UUID_DEAL BEFORE INSERT ON deals FOR EACH ROW SET NEW.uuid = UUID();")

        //s.orm.LogMode(false)
    }
}